<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usermodel extends CI_Model
{
	function token_to_id($access_token = '')
	{
		$where_token = array(
			'access_token' => $access_token
		);

		$user = $this->db->get_where('user', $where_token)->result();
		if (count($user) != 0)
		{
			return $user[0]->user_id;
		}
		else
		{
			return "null";
		}
	}

	function indonesia_datetime($datetime = '')
	{
		if ($datetime == "0000-00-00 00:00:00")
		{
			return "Update Belum Tersedia";
		}
		else
		{
			$new_datetime = date_format(date_create($datetime), "Y-m-d H:i");

			$split_datetime = explode(" ", $new_datetime);
			$date = $split_datetime[0];
			$time = $split_datetime[1];

			$month = array (
			1 =>'Januari',
				'Februari',
				'Maret',
				'April',
				'Mei',
				'Juni',
				'Juli',
				'Agustus',
				'September',
				'Oktober',
				'November',
				'Desember'
			);

			$split_date = explode('-', $date);
			return $split_date[2] . ' ' . $month[(int)$split_date[1]] . ' ' . $split_date[0] . '-' . $time . " WIB";
		}
	}
}
?>