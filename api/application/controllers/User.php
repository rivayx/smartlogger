<?php
	use Restserver\Libraries\REST_Controller;
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	require APPPATH . 'libraries/REST_Controller.php';
	require APPPATH . 'libraries/Format.php';

class User extends REST_Controller
{	
	function __construct($config = 'rest') 
	{
		parent::__construct($config);
		$this->load->model('usermodel');
	}

	public function register_post()
	{
		$email = $this->input->post('email');
		$password = md5($this->input->post('password'));
		$name = $this->input->post('name');
		$phone = $this->input->post('phone');
		$address = $this->input->post('address');
		
		$where_email = array(
			'email' => $email
		);

		$user = array(
			'email' => $email,
			'password' => $password,
			'name' => $name,
			'phone' => $phone,
			'address' => $address,
		);

		$status = "";
		$status_code = 0;
		$message = "";

		$data_email = $this->db->get_where('user', $where_email)->result();

		if (count($data_email) == 0)
		{
			$this->db->insert('user', $user);
			$status = "success";
			$status_code = 10;
			$message = "Registrasi berhasil.";
		}
		else
		{
			$status = "failed";
			$status_code = 11;
			$message = "Email telah terdaftar.";
		}

		$error = $this->db->error();
		if ($error['message'] == "")
		{
			$output = array(
				'status' => $status,
				'status_code' => $status_code,
				'message' => $message,
			);
		}
		else
		{
			$output = array(
				'status' => 'error',
				'status_code' => 999,
				'message' => $error['message']
			);
		}
		
		$this->response($output, 200);
	}

	public function authenticate_post()
	{
		$email = $this->input->post('email');
		$password = md5($this->input->post('password'));

		$where_email = array(
			'email' => $email
		);

		$user =  $this->db->get_where('user', $where_email)->result();

		if (count($user) > 0)
		{
			if ($user[0]->password == $password)
			{
				$update_token = array(
					'access_token' => md5($email . $password . $user[0]->user_id . date("Y-m-d H:i")), 
				);

				$this->db->where('user_id', $user[0]->user_id);
				$this->db->update('user', $update_token);

				$status = "success";
				$status_code = 10;
				$message = "Login Berhasil.";
			}
			else
			{
				$status = "failed";
				$status_code = 11;
				$message = "Kombinasi email dengan password tidak cocok.";
			}
		}
		else
		{
			$status = "failed";
			$status_code = 12;
			$message = "Email belum terdaftar.";
		}


		// $data = $this->db->get_where('user', $where_email)->result();
		// unset($data[0]->user_id);
		// unset($data[0]->password);

		$error = $this->db->error();
		if ($error['message'] == "")
		{
			$output = array(
				'status' => $status,
				'status_code' => $status_code,
				'message' => $message,
			);
			if ($status_code == 10)
			{
				$output['access_token'] = $update_token['access_token'];
			}
		}
		else
		{
			$output = array(
				'status' => 'error',
				'status_code' => 999,
				'message' => $error['message']
			);
		}
		
		$this->response($output, 200);
	}

	public function user_info_post()
	{
		$access_token = $this->input->post('access_token');

		$where_token = array(
			'access_token' => $access_token
		);

		$user = $this->db->get_where('user', $where_token)->result();
		if (count($user) > 0)
		{
			unset($user[0]->user_id);
			unset($user[0]->password);
			unset($user[0]->access_token);

			$status = "success";
			$status_code = 10;
			$message = "Sukses menampilkan info user.";
		}
		else
		{
			$status = "failed";
			$status_code = 99;
			$message = "Anda sudah logout dari aplikasi. Silahkan login kembali dan ulangi perintah.";
		}

		$error = $this->db->error();
		if ($error['message'] == "")
		{
			$output = array(
				'status' => $status,
				'status_code' => $status_code,
				'message' => $message,
			);
			if ($status_code == 10)
			{
				$output['user_info'] = $user[0];
			}
		}
		else
		{
			$output = array(
				'status' => 'error',
				'status_code' => 999,
				'message' => $error['message']
			);
		}
		
		$this->response($output, 200);
	}

	public function edit_user_post()
	{
		$access_token = $this->input->post('access_token');
		$email = $this->input->post('email');
		$password = md5($this->input->post('password'));
		$name = $this->input->post('name');
		$phone = $this->input->post('phone');
		$address = $this->input->post('address');

		$user_id = $this->usermodel->token_to_id($access_token);

		if ($user_id != "null")
		{
			$update_user = array(
				'password' => $password,
				'name' => $name,
				'phone' => $phone,
				'email' => $email,
				'address' => $address,
			);

			$this->db->where('user_id', $user_id);
			$this->db->update('user', $update_user);

			$status = "success";
			$status_code = 10;
			$message = "Berhasil merubah data.";
		}
		else
		{
			$status = "failed";
			$status_code = 99;
			$message = "Anda sudah logout dari aplikasi. Silahkan login kembali dan ulangi perintah.";
		}


		$error = $this->db->error();
		if ($error['message'] == "")
		{
			$output = array(
				'status' => $status,
				'status_code' => $status_code,
				'message' => $message,
			);
		}
		else
		{
			$output = array(
				'status' => 'error',
				'status_code' => 999,
				'message' => $error['message']
			);
		}
		
		$this->response($output, 200);
	}
}