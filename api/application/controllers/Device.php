<?php
	use Restserver\Libraries\REST_Controller;
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	require APPPATH . 'libraries/REST_Controller.php';
	require APPPATH . 'libraries/Format.php';

class Device extends REST_Controller
{	
	function __construct($config = 'rest') 
	{
		parent::__construct($config);
		$this->load->model('usermodel');
	}
//========================================================
//======================= For Apps =======================
//========================================================

	public function add_user_device_post()
	{
		$access_token = $this->input->post('access_token');
		$device_id = $this->input->post('device_id');
		$device_name = $this->input->post('device_name');

		$user_id = $this->usermodel->token_to_id($access_token);

		if ($user_id != "null")
		{
			$where_device_id = array(
				'device_id' => $device_id,
			);

			$data_device =  $this->db->get_where('device', $where_device_id)->result();

			if (count($data_device) > 0)
			{
				$user_device = array(
					'user_id' => $user_id,
					'device_id' => $device_id,
				);

				$data_user_device =  $this->db->get_where('user_device', $user_device)->result();

				if (count($data_user_device) == 0)
				{
					if ($device_name != "")
					{
						$user_device['status'] = "show";
						$user_device['time_create'] = date("Y-m-d H:i:s");
						$user_device['time_update'] = date("Y-m-d H:i:s");
						
						$this->db->insert('user_device', $user_device);


						$update_device = array(
							'device_name' => $device_name,
						);

						$this->db->where('device_id', $device_id);
						$this->db->update('device', $update_device);

						$status = "success";
						$status_code = 10;
						$message = $device_name . "(". $device_id . ") berhasil ditambahkan ke perangkat Anda.";
					}
					else
					{
						$status = "failed";
						$status_code = 12;
						$message = "Nama Perangkat harus diisi.";
					}					
				}
				else
				{
					if ($data_user_device[0]->status == "hide")
					{						
						$this->db->where('device_id', $device_id);

						$user_device['status'] = "show";
						$user_device['time_update'] = date("Y-m-d H:i:s");

						$this->db->update('device', $data_update);


						$status = "success";
						$status_code = 10;
						$message = $device_name . "(". $device_id . ") berhasil ditambahkan kembali ke perangkat Anda.";
					}
					else
					{
						$status = "failed";
						$status_code = 11;
						$message = $device_name . "(". $device_id . ") telah terdaftar di perangkat Anda.";
					}
				}			
			}
			else
			{
				$status = "failed";
				$status_code = 13;
				$message = "Device tidak tersedia.";
			}
		}
		else
		{
			$status = "failed";
			$status_code = 99;
			$message = "Anda sudah logout dari aplikasi. Silahkan login kembali dan ulangi perintah.";
		}

		$error = $this->db->error();
		if ($error['message'] == "")
		{
			$output = array(
				'status' => $status,
				'status_code' => $status_code,
				'message' => $message,
			);
		}
		else
		{
			$output = array(
				'status' => 'error',
				'status_code' => 999,
				'message' => $error['message']
			);
		}
		
		$this->response($output, 200);
	}

	public function get_user_device_post()
	{
		$access_token = $this->input->post('access_token');

		$user_id = $this->usermodel->token_to_id($access_token);

		if ($user_id != "null")
		{			
			$where_user = array(
				'user_id' => $user_id, 
			);

			$data_user_device = $this->db->get_where('user_device', $where_user)->result();

			if (count($data_user_device) > 0)
			{
				$data_output = array();
				for ($i = 0; $i < count($data_user_device); $i++)
				{
					$where_device = array(
						'device_id' => $data_user_device[$i]->device_id,
					);

					$data_output[$i] = $this->db->get_where('device', $where_device)->result()[0];
					$data_output[$i] = json_decode(json_encode($data_output[$i]), true); //CONVERT TO ARRAY

					$this->db->select('date_time as last_update, monitoring_id');
					$this->db->order_by('date_time', 'desc');
					$data_output[$i]["last_monitoring"] = $this->db->get_where('monitoring', $where_device)->result()[0];
					$data_output[$i]["last_monitoring"] = json_decode(json_encode($data_output[$i]["last_monitoring"]), true); //CONVERT TO ARRAY

					$this->db->select('sensor_name, value as val');
					$this->db->where('monitoring_id', $data_output[$i]["last_monitoring"]["monitoring_id"]);
					$data_output[$i]["last_monitoring"]["monitoring_detail"] = $this->db->get('monitoring_detail')->result();
					$data_output[$i]["last_monitoring"]["monitoring_detail"] = json_decode(json_encode($data_output[$i]["last_monitoring"]["monitoring_detail"]), true); //CONVERT TO ARRAY

					for ($j = 0; $j < count($data_output[$i]["last_monitoring"]["monitoring_detail"]); $j++) 
					{ 
						$this->db->select('satuan');
						$this->db->where('sensor_name', $data_output[$i]["last_monitoring"]["monitoring_detail"][$j]["sensor_name"]);
						$data_output[$i]["last_monitoring"]["monitoring_detail"][$j]["satuan"] = $this->db->get('sensor')->result()[0]->satuan;
					}
				}

				$status = "success";
				$status_code = 10;
				$message = "Menampilkan data.";
			}
			else
			{
				$status = "failed";
				$status_code = 11;
				$message = "Anda belum mendaftarkan device.";
			}
		}
		else
		{
			$status = "failed";
			$status_code = 99;
			$message = "Anda sudah logout dari aplikasi. Silahkan login kembali dan ulangi perintah.";
		}

		$error = $this->db->error();
		if ($error['message'] == "")
		{
			$output = array(
				'status' => $status,
				'status_code' => $status_code,
				'message' => $message,
			);
			if ($status_code == 10)
			{
				$output['data'] = $data_output;
			}
		}
		else
		{
			$output = array(
				'status' => 'error',
				'status_code' => 999,
				'message' => $error['message']
			);
		}
		
		$this->response($output, 200);
	}

	







			



//oke
	public function edit_device_name_post()
	{
		$access_token = $this->input->post('access_token');
		$device_id = $this->input->post('device_id');
		$device_name = $this->input->post('device_name');

		$user_id = $this->usermodel->token_to_id($access_token);

		if ($user_id != "null")
		{
			$where_device_id = array(
				'device_id' => $device_id,
			);

			$data_device =  $this->db->get_where('device', $where_device_id)->result();

			if (count($data_device) > 0)
			{
				if ($device_name != "")
				{
					$update_device = array(
						'device_name' => $device_name,
					);

					$this->db->where('device_id', $device_id);
					$this->db->update('device', $update_device);

					$status = "success";
					$status_code = 10;
					$message = "Berhasil merubah nama perangkat.";

				}
				else
				{
					$status = "failed";
					$status_code = 11;
					$message = "Nama Perangkat harus diisi.";
				}
			}
			else
			{
				$status = "failed";
				$status_code = 98;
				$message = "Device tidak tersedia.";
			}
		}
		else
		{
			$status = "failed";
			$status_code = 99;
			$message = "Anda sudah logout dari aplikasi. Silahkan login kembali dan ulangi perintah.";
		}

		$error = $this->db->error();
		if ($error['message'] == "")
		{
			$output = array(
				'status' => $status,
				'status_code' => $status_code,
				'message' => $message,
			);
		}
		else
		{
			$output = array(
				'status' => 'error',
				'status_code' => 999,
				'message' => $error['message']
			);
		}
		
		$this->response($output, 200);
	}


//oke
	public function send_data_now_post()
	{
		$access_token = $this->input->post('access_token');
		$device_id = $this->input->post('device_id');

		$user_id = $this->usermodel->token_to_id($access_token);

		if ($user_id != "null")
		{
			$this->db->where('device_id', $device_id);
			$data_device = $this->db->get('device')->result();

			if (count($data_device) > 0)
			{
				$update_device = array(
					'send_data_now' => 'true',
				);

				$this->db->where('device_id', $device_id);
				$this->db->update('device', $update_device);

				$status = "success";
				$status_code = 10;
				$message = "Berhasil meminta data terbaru.";
			}
			else
			{
				$status = "failed";
				$status_code = 98;
				$message = "Device tidak tersedia.";
			}

		}
		else
		{
			$status = "failed";
			$status_code = 99;
			$message = "Anda sudah logout dari aplikasi. Silahkan login kembali dan ulangi perintah.";
		}

		$error = $this->db->error();
		if ($error['message'] == "")
		{
			$output = array(
				'status' => $status,
				'status_code' => $status_code,
				'message' => $message,
			);
		}
		else
		{
			$output = array(
				'status' => 'error',
				'status_code' => 999,
				'message' => $error['message']
			);
		}
		$this->response($output, 200);
	}


//oke
	public function update_minmax_sensor_post()
	{
		$input = json_decode($this->input->raw_input_stream, true);

		$access_token = $input['access_token'];
		$device_sensor_id = $input['device_sensor_id'];
		$min = $input['min'];
		$max = $input['max'];

		$user_id = $this->usermodel->token_to_id($access_token);

		if ($user_id != "null")
		{
			$update_minmax = array(
				'min' => $min,
				'max' => $max,
			);

			$this->db->where('device_sensor_id', $device_sensor_id);
			$this->db->update('device_sensor', $update_minmax);

			$status = "success";
			$status_code = 10;
			$message = "Berhasil mengirim nilai min dan max.";

		}
		else
		{
			$status = "failed";
			$status_code = 99;
			$message = "Anda sudah logout dari aplikasi. Silahkan login kembali dan ulangi perintah.";
		}

		$error = $this->db->error();
		if ($error['message'] == "")
		{
			$output = array(
				'status' => $status,
				'status_code' => $status_code,
				'message' => $message,
			);
		}
		else
		{
			$output = array(
				'status' => 'error',
				'status_code' => 999,
				'message' => $error['message']
			);
		}
		$this->response($output, 200);
	}

//========================================================
//====================== For Device ======================
//========================================================

//oke
	public function wifi_connected_post()
	{
		$input = json_decode($this->input->raw_input_stream, true);

		$new_connection = array(
			'device_id' => $input["device_id"],
			'wifi_ssid' => $input["ssid"],
			'wifi_signal' => $input["signal"],
			'ip_address' => $input["ip_address"],
			'date_time' => date("Y-m-d H:i:s"),
		);

		$this->db->insert('connection_device', $new_connection);

		$status = "success";
		$status_code = 10;
		$message = "Berhasil mengirim data WiFi.";

		$error = $this->db->error();
		if ($error['message'] == "")
		{
			$output = array(
				'status' => $status,
				'status_code' => $status_code,
				'message' => $message,
			);
		}
		else
		{
			$output = array(
				'status' => 'error',
				'status_code' => 999,
				'message' => $error['message']
			);
		}
		$this->response($output, 200);
	}


//oke
	public function register_sensor_post()
	{
		$input = json_decode($this->input->raw_input_stream, true);	
		$device_id = $input["device_id"];
		$sensor_name = $input["sensor_name"];

		if ($device_id != "" && $sensor_name != "")
		{
			$new_sensor = array(
				'device_id' => $device_id,
				'sensor_name' => $sensor_name,
				'min' => $input["min"],
				'max' => $input["max"],
			);

			$this->db->insert('device_sensor', $new_sensor);

			$status = "success";
			$status_code = 10;
			$message = "Berhasil menambahkan sensor ke perangkat.";
		}
		else
		{
			$status = "failed";
			$status_code = 98;
			$message = "device_id dan sensor_name harus diisi";
		}

		$error = $this->db->error();
		if ($error['message'] == "")
		{
			$output = array(
				'status' => $status,
				'status_code' => $status_code,
				'message' => $message,
			);
		}
		else
		{
			$output = array(
				'status' => 'error',
				'status_code' => 999,
				'message' => $error['message']
			);
		}
		$this->response($output, 200);
	}


//oke
	public function add_monitoring_post()
	{
		$input = json_decode($this->input->raw_input_stream, true);	
		$device_id = $input["device_id"];
		$date_time = date("Y-m-d H:i:s");

		$new_monitoring = array(
			'date_time' =>$date_time,
		);

		$this->db->insert('monitoring', $new_monitoring);
		$monitoring_id = $this->db->get_where('monitoring', $new_monitoring)->result()[0]->monitoring_id;

		for ($i = 0; $i < count($input["sensor"]); $i++)
		{
			$device_sensor_id = $input["sensor"][$i]["device_sensor_id"];
			$value = $input["sensor"][$i]["value"];

			$new_monitoring_detail = array(
				'monitoring_id' => $monitoring_id,
				'device_sensor_id' => $device_sensor_id,
				'value' => $value,
			);

			$this->db->insert('monitoring_detail', $new_monitoring_detail);
		}

		$update_device = array(
			'send_data_now' => 'false',
		);

		$this->db->where('device_id', $device_id);
		$this->db->update('device', $update_device);

		$status = "success";
		$status_code = 10;
		$message = "Berhasil mengirim data perangkat.";

		$error = $this->db->error();
		if ($error['message'] == "")
		{
			$output = array(
				'status' => $status,
				'status_code' => $status_code,
				'message' => $message,
			);
		}
		else
		{
			$output = array(
				'status' => 'error',
				'status_code' => 999,
				'message' => $error['message']
			);
		}
		$this->response($output, 200);
	}

	public function get_data_device_post() //LAST UPDATE
	{
		$input = json_decode($this->input->raw_input_stream, true);	
		$device_id = $input["device_id"];

		$this->db->where('device_id', $device_id);
		$data_device = $this->db->get('device')->result()[0];
		$data_device = json_decode(json_encode($data_device), true);
		$data_device["send_data_now"] = $data_device["send_data_now"] == "true";

		$this->db->select('wifi_ssid, ip_address, wifi_signal, date_time as last_connected');
		$this->db->where('device_id', $device_id);
		$this->db->order_by('date_time', 'desc');
		$data_device["connection"] = $this->db->get('connection_device')->result()[0];

		$this->db->select('device_sensor_id, sensor_name, min, max');
		$this->db->where('device_id', $device_id);
		$device_sensor = $this->db->get('device_sensor')->result();
		$device_sensor = json_decode(json_encode($device_sensor), true);

		for ($i = 0; $i < count($device_sensor); $i++) 
		{ 
			$this->db->order_by('monitoring_id', 'desc');
			$this->db->where('device_sensor_id', $device_sensor[$i]["device_sensor_id"]);
			$monitoring_detail = $this->db->get('monitoring_detail')->result()[0];
			$device_sensor[$i]["value"] = $monitoring_detail->value;

			$this->db->where('sensor_name', $device_sensor[$i]["sensor_name"]);
			$device_sensor[$i]["satuan"] = $this->db->get('sensor')->result()[0]->satuan;

			$this->db->where('monitoring_id', $monitoring_detail->monitoring_id);
			$data_device["last_update"] = $this->db->get('monitoring')->result()[0]->date_time;
		}

		$data_device["device_sensor"] = $device_sensor;
		
		$status = "success";
		$status_code = 10;
		$message = "Berhasil mengambil data perangkat.";

		$error = $this->db->error();
		if ($error['message'] == "")
		{
			$output = array(
				'status' => $status,
				'status_code' => $status_code,
				'message' => $message,
				'data' => $data_device,
			);
		}
		else
		{
			$output = array(
				'status' => 'error: '.$error['message'],
				'status_code' => 999,
			);
		}
		$this->response($output, 200);
	}


	public function monitoring_detail_post()
	{		
		$input = json_decode($this->input->raw_input_stream, true);
		$access_token = $input["access_token"];
		$device_sensor_id = $input["device_sensor_id"];
		$start_date = $input["start_date"];
		$end_date = $input["end_date"];

		if ($start_date != "")
			$start_date = $start_date . " 00:00:00";
		else
			$start_date = "2000-01-01 00:00:00";

		$end_date = $end_date . " 23:59:59";

		$user_id = $this->usermodel->token_to_id($access_token);

		if ($user_id != "null")
		{
		
			$where_device_id = array(
				'device_sensor_id' => $device_sensor_id,
			);

			$this->db->order_by('sensor.sensor_name', 'desc');
			$this->db->select('device_sensor_id, sensor.sensor_name, min, max, satuan');
			$this->db->join('sensor', 'sensor.sensor_name = device_sensor.sensor_name', 'inner');
			$device_sensor = $this->db->get_where('device_sensor', $where_device_id)->result();
			$device_sensor = json_decode(json_encode($device_sensor), true);

			for ($i = 0; $i < count($device_sensor); $i++) 
			{ 
				$this->db->select('monitoring_detail.value, monitoring.date_time');
				$this->db->join('monitoring_detail', 'monitoring.monitoring_id = monitoring_detail.monitoring_id', 'inner');
				$this->db->where('monitoring_detail.device_sensor_id', $device_sensor[$i]["device_sensor_id"]);
				$this->db->where('monitoring.date_time BETWEEN "'. date("Y-m-d H:i:s", strtotime($start_date)).'" AND "'.date("Y-m-d H:i:s", strtotime($end_date)).'"');
				$this->db->order_by('monitoring.date_time', 'DESC');
				$device_sensor[$i]["monitoring_detail"] = $this->db->get('monitoring')->result();
			}
		
			$status = "success";
			$status_code = 10;
			$message = "Berhasil mengambil data perangkat.";
		}
		else
		{
			$status = "failed";
			$status_code = 99;
			$message = "Anda sudah logout dari aplikasi. Silahkan login kembali dan ulangi perintah.";
			$device_sensor = "";
		}


		$error = $this->db->error();
		if ($error['message'] == "")
		{
			$output = array(
				'status' => $status,
				'status_code' => $status_code,
				'message' => $message,
				'data' => $device_sensor[0],
			);
		}
		else
		{
			$output = array(
				'status' => 'error: '.$error['message'],
				'status_code' => 999,
			);
		}
		$this->response($output, 200);
	}
}