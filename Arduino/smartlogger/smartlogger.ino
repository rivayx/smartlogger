#include <Preferences.h>
#include <BluetoothSerial.h>
#include <ArduinoJson.h>

#include "SmartloggerJSONClass.h"
#include "SmartloggerManager.h"
#include "SmartloggerConnection.h"

#define maxRetryConnect 3

Preferences preferences;
BluetoothSerial SerialBT;

String deviceID = "smartlogger_00001";


DeviceInfo device(deviceID);
WiFiConnection wifi;
SerialInputJsonClass inputJson;


unsigned long millisSecond;

unsigned long waitRetryConnection;
unsigned long intervalRetryConnection = 10;

unsigned long waitRefresh;
unsigned long intervalRefresh = 10;

unsigned long waitSendData;
unsigned long intervalSendData = 600;

bool isConnecting = false;
bool isConnectToWifi = false;
int countRetryConnect;

String inputString = "";
int statusTemp;

String ssid;
String password;



void setup()
{
  Serial.begin(115200);
  SerialBT.begin(deviceID);
  wifi.deviceID = deviceID;  
  preferences.begin("deviceInfo");
  
  countRetryConnect = 0;  
  isConnectToWifi = false;
  if (preferences.getString("ssid") != "")
  {
    ssid = preferences.getString("ssid");
    password = preferences.getString("password");
    Print("Auto Connect to ");
    Println(ssid);
    isConnecting = true;
  }
  
  statusTemp = 999;
  waitRetryConnection = intervalRetryConnection;
  waitRefresh = intervalRefresh;
  waitSendData = intervalSendData;
}

void loop()
{  
  millisSecond = millis() / 1000;
  
  SerialCommunication();
  ArduinoWifiConncetion();
}



void SerialCommunication()
{  
  if (Serial.available())
  {
    inputString = Serial.readStringUntil('\n');
  }

  if (SerialBT.available())
  {
    inputString = SerialBT.readStringUntil('\n');
  }

  if (inputString != "")
  {
    Serial.print("input :: ");
    Serial.println(inputString);
    delay(200);
    inputJson.Parse(inputString);
    inputString = "";
    
    switch (inputJson.code) 
    {
      case CodeCommunication().scanWifi:
        Println("scan wifi...");
        Println(wifi.GetWifiListJSON());
        break;
        
      case CodeCommunication().connectWifi:
        delay(500);
        Print("start connect to ");
        Println(inputJson.ssid);
        ssid = String(inputJson.ssid);
        password = String(inputJson.password);
        isConnecting = true;
        delay(500);
        wifi.Connect(inputJson.ssid, inputJson.password);
        break;
    }
  }
}

void ArduinoWifiConncetion()
{
  if (statusTemp != wifi.Status())
  {
    Print("Status : ");
    Println(wifi.StatusInfo());
    statusTemp = wifi.Status();
  }
  
  if (wifi.Status() == 3) //CONNECT TO WIFI
  {
    isConnecting = false;
    if (!isConnectToWifi) //FIRST CONNECTION
    {
      isConnectToWifi = true;
      countRetryConnect = 0;
      preferences.remove("ssid");
      preferences.remove("password");
      preferences.clear();
      delay(200);
      preferences.putString("ssid", ssid);
      preferences.putString("password", password);
      preferences.end();
      
      Println(wifi.FirstConnectionJSON());
      Println(wifi.SendConnectionToServer());

      device.Refresh(true);
      delay(500);
      
      waitRefresh = millisSecond + intervalRefresh;
      waitSendData = millisSecond + intervalSendData;
    }
    else
    {
      if (millisSecond > waitRefresh) //DELAY
      {
        device.Refresh(false);
        waitRefresh = millisSecond + intervalRefresh;
      }
      
      if (millisSecond > waitSendData) //DELAY
      {
        device.Refresh(true);
        Println(wifi.SendConnectionToServer());
        waitSendData = millisSecond + intervalSendData;
      }
    }
  }
  else
  {
    isConnectToWifi = false;
    if (isConnecting)
    {
      if (wifi.Status() != 0)
      {
        if (countRetryConnect < maxRetryConnect) //RETRY CONNECTION
        {
          if (millisSecond > waitRetryConnection) //DELAY
          {
            delay(100);
            inputString = wifi.ConnectWifiJSON(ssid, password);
            countRetryConnect++;
            waitRetryConnection = millisSecond + intervalRetryConnection;
          }
        }
        else //CONNECT FAILED
        {  
          isConnecting = false;
          Println(wifi.FailedConnectWoWifi());
          countRetryConnect = 0;
        }
      }
    }
  }
}


//===========================================================================
//============================ SERIAL PRINT =================================
//===========================================================================


void Print(bool message)
{
  Serial.print(message);
  SerialBT.print(message);
}

void Print(float message)
{
  Serial.print(message);
  SerialBT.print(message);
}

void Print(int message)
{
  Serial.print(message);
  SerialBT.print(message);
}

void Print(const char* message)
{
  Serial.print(message);
  SerialBT.print(message);
}

void Print(char* message)
{
  Serial.print(message);
  SerialBT.print(message);
}

void Print(String message)
{
  Serial.print(message);
  SerialBT.print(message);
}




void Println(bool message)
{
  Serial.println(message);
  SerialBT.println(message);
}

void Println(float message)
{
  Serial.println(message);
  SerialBT.println(message);
}

void Println(int message)
{
  Serial.println(message);
  SerialBT.println(message);
}

void Println(const char* message)
{
  Serial.println(message);
  SerialBT.println(message);
}

void Println(char* message)
{
  Serial.println(message);
  SerialBT.println(message);
}

void Println(String message)
{
  Serial.println(message);
  SerialBT.println(message);
}
