
#ifndef SMARTLOGGER_JSONCLASS_H
#define SMARTLOGGER_JSONCLASS_H

#include <ArduinoJson.h>
#include "SmartloggerManager.h"
#include "SmartloggerConnection.h"

class SerialInputJsonClass
{
public:
  int code;
  const char* ssid;
  const char* password;
  
  void Parse(String jsonString)  
  {
    DynamicJsonDocument JSONParse(256);
    delay(100);
    deserializeJson(JSONParse, jsonString);
    delay(100);
    this->code = JSONParse["code"];
    if (this->code == 2)
    {
      this->ssid = JSONParse["ssid"];
      this->password= JSONParse["password"];    
    }
  };  
};
#endif
