
#ifndef SMARTLOGGER_MANAGER_H
#define SMARTLOGGER_MANAGER_H

#include <Arduino.h>
#include "ArduinoJson.h"
#include "SmartloggerConnection.h"


struct UrlManager
{
public:
  const String host = 
      //"https://pondokhidroponik.com/smartponic/api/device/"; //LIVE
      "http://192.168.100.2/smartlogger/api/"; //LOCAL

  const String wifi_connected = host + "device/wifi_connected";
  const String get_data_device = host + "device/get_data_device";
  const String add_monitoring = host + "device/add_monitoring";
};


class SensorValue
{
public:    
  SensorValue();
  ~SensorValue();
  
  float GetValue(String sensorName);
};


class DeviceSensor
{
public:
  int id;
  float value;
  String sensorName;
  float min;
  float max;
  String satuan;
};

class Monitoring
{
public:
  static String Send(String deviceID, int countSensor, DeviceSensor listSensor[])
  {
    StaticJsonDocument<512> request;  
    request["device_id"] = deviceID;
    JsonArray data = request.createNestedArray("sensor");
    StaticJsonDocument<256> sensor;
    
    for (int i = 0; i < countSensor ; i++)
    {
      sensor["device_sensor_id"] = listSensor[i].id;
      sensor["value"] = listSensor[i].value;
      
      data.add(sensor);
      sensor.clear();
    }
    
    String jsonString;
    serializeJson(request, jsonString);
    request.clear();

    return WebRequest().Post(UrlManager().add_monitoring, jsonString);
  }
};

class DeviceInfo
{
public:
  String deviceID;
  String versionID;
  String deviceName;
  bool sendDataNow;
  String lastUpdate;
  int countSensor;
  DeviceSensor* listSensor;
  
  DeviceInfo(String deviceID)
  {
    this->deviceID = deviceID;
  }
  
  void Refresh(bool sendData)
  {
    StaticJsonDocument<64> request;  
    request["device_id"] = deviceID;
    String jsonString;
    serializeJson(request, jsonString);
    request.clear();

    String response = WebRequest().Post(UrlManager().get_data_device, jsonString);
    
    Serial.println(response);
    StaticJsonDocument<1024> JSONParse;
    delay(100);
    deserializeJson(JSONParse, response);
    delay(100);
    
    JsonObject data = JSONParse["data"];
    this->versionID = data["version_id"].as<String>();
    this->deviceName = data["device_name"].as<String>();
    this->sendDataNow = data["send_data_now"].as<bool>();
    this->lastUpdate = data["last_update"].as<String>();

    if (!this->sendDataNow)
    {
      this->sendDataNow = sendData;
    }

    JsonArray sensor = data["device_sensor"];
    this->countSensor = sensor.size();
    DeviceSensor* listSensor = new DeviceSensor[countSensor];

    SensorValue sensorValue;
    for(int i = 0; i < this->countSensor; i++)
    {
      listSensor[i].id = sensor[i]["device_sensor_id"].as<int>();
      listSensor[i].sensorName = sensor[i]["sensor_name"].as<String>();
      listSensor[i].value = sensorValue.GetValue(listSensor[i].sensorName);
      listSensor[i].min = sensor[i]["min"].as<float>();
      listSensor[i].max = sensor[i]["max"].as<float>();
      listSensor[i].satuan = sensor[i]["satuan"].as<String>();

      if (listSensor[i].min > listSensor[i].value)
      {
        Serial.println("Value Melewati batas Minimum");
      }
      if (listSensor[i].max < listSensor[i].value)
      {
        Serial.println("Value Melewati batas Maximum");
      }
    }
    JSONParse.clear();

    if (this->sendDataNow)
    {
      Serial.println("Sending data...");
      Serial.println(Monitoring().Send(deviceID, countSensor, listSensor));
    }
  }
  
  
};


#endif
