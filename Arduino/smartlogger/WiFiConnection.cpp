#include <WiFi.h>
#include <ArduinoJson.h>

#include "SmartloggerManager.h"
#include "SmartloggerConnection.h"

WiFiConnection::WiFiConnection()
{}

WiFiConnection::~WiFiConnection()
{}

void WiFiConnection::Connect(const char* ssid, const char* password)
{
  WiFi.mode(WIFI_STA);
  WiFi.disconnect();
  WiFi.begin(ssid, password);
}

String WiFiConnection::ConnectWifiJSON(String ssid, String password)
{
  StaticJsonDocument<256> JSONDocument;
  JSONDocument["code"] = 2;
  JSONDocument["ssid"] = ssid;
  JSONDocument["password"] = password;
  
  String jsonString;
  serializeJson(JSONDocument, jsonString);
  JSONDocument.clear();

  return jsonString;
}

String WiFiConnection::FailedConnectWoWifi()
{
  StaticJsonDocument<256> JSONDocument;
  JSONDocument["code"] = 999;
  JSONDocument["status"] = "Error";
  JSONDocument["message"] = "Gagal menyambungkan ke WiFi";
  
  String jsonString;
  serializeJson(JSONDocument, jsonString);
  JSONDocument.clear();

  return jsonString;
}

String WiFiConnection::FirstConnectionJSON()
{
  int wifiSignal = WiFi.RSSI() > (-50) ? 4 :
                   WiFi.RSSI() > (-60) ? 3 :
                   WiFi.RSSI() > (-70) ? 2 : 1 ;

  StaticJsonDocument<1024> JSONDocument;
  JSONDocument["json_type"] = "connected_wifi";
  JSONDocument["message"] = "Berhasil menghubungkan ke " + WiFi.SSID();
  JSONDocument["ip_address"] = WiFi.localIP().toString();
  JSONDocument["signal"] = wifiSignal;

  String jsonString;
  serializeJson(JSONDocument, jsonString);
  JSONDocument.clear();

  return jsonString;
}

String WiFiConnection::GetWifiListJSON()
{
	WiFi.mode(WIFI_STA);
	WiFi.disconnect();

	StaticJsonDocument<512> WifiListJSON;
	StaticJsonDocument<1024> JSONDocument;
	JSONDocument["json_type"] = "list_wifi_found";
	JSONDocument["message"] = "Daftar Wifi yang tersedia";

	JsonArray data = JSONDocument.createNestedArray("data");
	for (int i = 0; i < WiFi.scanNetworks(); i++)
	{
		WifiListJSON["ssid"] = WiFi.SSID(i);

    WifiListJSON["signal"] = WiFi.RSSI(i) > (-50) ? 4 :
                             WiFi.RSSI() > (-60) ? 3 :
                             WiFi.RSSI() > (-70) ? 2 : 1 ;
		
		WifiListJSON["rssi"] = WiFi.RSSI(i);
		WifiListJSON["password"] = (WiFi.encryptionType(i) == WIFI_AUTH_OPEN) ? false : true;

		data.add(WifiListJSON);
		WifiListJSON.clear();
	}

	String jsonString;
	serializeJson(JSONDocument, jsonString);
	JSONDocument.clear();

	return jsonString;
}

String WiFiConnection::SendConnectionToServer()
{
  StaticJsonDocument<512> request;  
  request["device_id"] = this->deviceID;
  request["ssid"] = WiFi.SSID();
  request["ip_address"] = WiFi.localIP().toString();
  request["signal"] = WiFi.RSSI() > (-50) ? 4 :
                      WiFi.RSSI() > (-60) ? 3 :
                      WiFi.RSSI() > (-70) ? 2 : 1 ;
                      
  String jsonString;
  serializeJson(request, jsonString);
  request.clear();
  
  return WebRequest().Post(UrlManager().wifi_connected, jsonString);
}

int WiFiConnection::Status() 
{  
  return WiFi.status();
}

String WiFiConnection::StatusInfo()
{
  switch (WiFi.status())
  {
    case 255:
      return "no status";
      break;
    case 0:
      return "0 - WL_IDLE_STATUS - temporary status assigned when WiFi.begin() is called";
      break;
    case 1:
      return "1 - WL_NO_SSID_AVAIL - when no SSID are available";
      break;
    case 2:
      return "2 - WL_SCAN_COMPLETED - scan networks is completed";
      break;
    case 3:
      return "3 - WL_CONNECTED - when connected to a WiFi network";
      break;
    case 4:
      return "4 - WL_CONNECT_FAILED - when the connection fails for all the attempts";
      break;
    case 5:
      return "5 - WL_CONNECTION_LOST - when the connection is lost";
      break;
    case 6:
      return "6 - WL_DISCONNECTED - when disconnected from a network";
      break;
  }
}
