#include <Arduino.h>
#include "SmartloggerManager.h"


SensorValue::SensorValue()
{}

SensorValue::~SensorValue()
{}

float SensorValue::GetValue(String sensorName)
{
  float value = 0;

  if (sensorName == "temperature")
  {
    value = random(28,31);
  }
  
  if (sensorName == "humidity")
  {
    value = random(61,81);
  }
  
  return value;
}
