#ifndef SMARTLOGGER_CONNECTION_H
#define SMARTLOGGER_CONNECTION_H

#include <Arduino.h>
#include <WiFi.h>
#include <HTTPClient.h>
#include <ArduinoJson.h>

#include "SmartloggerJSONClass.h"

struct CodeCommunication
{
public:
  const int scanWifi = 1;
  const int connectWifi = 2;

};

class WebRequest
{
public:  
	static String Post(String url, String jsondata)
	{
    HTTPClient http;
    http.begin(url); 
    http.addHeader("Content-Type", "application/json"); 
  
    int httpResponseCode = http.POST(jsondata); 
  
    String response = httpResponseCode > 0 ? 
           http.getString() :
           "Error: " + http.errorToString(httpResponseCode) + " (" + url + ")";
  
    return response;
	}
};

class WiFiConnection
{
public:
	WiFiConnection();
	~WiFiConnection();

  String deviceID;
  
  void Connect(const char* ssid, const char* password);
  
  String ConnectWifiJSON(String ssid, String password);
  String SendConnectionToServer();
  String FirstConnectionJSON();
  String FailedConnectWoWifi();
	String GetWifiListJSON();
  int Status();
  String StatusInfo();
	
};
#endif
