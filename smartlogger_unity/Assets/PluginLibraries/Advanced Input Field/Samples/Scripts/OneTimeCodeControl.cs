﻿using UnityEngine;
using UnityEngine.UI;

namespace AdvancedInputFieldPlugin.Samples
{
	public class OneTimeCodeControl: MonoBehaviour
	{
		[SerializeField]
		private Text instructionLabel;

		private void Awake()
		{
			instructionLabel.gameObject.SetActive(false);
		}

		public void OnRequestCodeClick()
		{
#if UNITY_ANDROID
			NativeKeyboardManager.StartListeningForOneTimeCodes(); //Only needed for Android
#endif
			instructionLabel.gameObject.SetActive(true);
		}
	}
}
