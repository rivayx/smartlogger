﻿//-----------------------------------------
//			Advanced Input Field
// Copyright (c) 2017 Jeroen van Pienbroek
//------------------------------------------

using System.Collections.Generic;
using System.Text;
using UnityEditor;
using UnityEditor.AnimatedValues;
using UnityEngine;
using UnityEngine.UI;

namespace AdvancedInputFieldPlugin.Editor
{
	[CustomEditor(typeof(AdvancedInputField), true)]
	public class AdvancedInputFieldEditor: UnityEditor.Editor
	{
		private const string DEPRECATED_FORMAT_RESOURCE_PATH = "AdvancedInputField/Images/deprecated_format";
		private const string UNITY_FORMAT_IMAGE_PATH = "AdvancedInputField/Images/unity_format";
		private const string TEXTMESHPRO_FORMAT_IMAGE_PATH = "AdvancedInputField/Images/textmeshpro_format";

		private SerializedProperty interactableProperty; //Property from Selectable base class
		private SerializedProperty targetGraphicProperty;
		private SerializedProperty transitionProperty;
		private SerializedProperty colorBlockProperty;
		private SerializedProperty spriteStateProperty;
		private SerializedProperty animTriggerProperty;
		private AnimBool showColorTint = new AnimBool();
		private AnimBool showSpriteTrasition = new AnimBool();
		private AnimBool showAnimTransition = new AnimBool();

		private SerializedProperty modeProperty;
		private SerializedProperty textProperty;
		private SerializedProperty placeholderTextProperty;
		private SerializedProperty richTextEditingProperty;
		private SerializedProperty richTextConfigProperty;
		private SerializedProperty characterLimitProperty;
		private SerializedProperty lineLimitProperty;
		private SerializedProperty contentTypeProperty;
		private SerializedProperty lineTypeProperty;
		private SerializedProperty inputTypeProperty;
		private SerializedProperty visiblePasswordProperty;
		private SerializedProperty keyboardTypeProperty;
		private SerializedProperty characterValidationProperty;
		private SerializedProperty characterValidatorProperty;
		private SerializedProperty emojisAllowedProperty;
		private SerializedProperty liveProcessingFilterProperty;
		private SerializedProperty postProcessingFilterProperty;
		private SerializedProperty selectionModeProperty;
		private SerializedProperty dragModeProperty;
		private SerializedProperty caretOnBeginEditProperty;
		private SerializedProperty caretBlinkRateProperty;
		private SerializedProperty caretWidthProperty;
		private SerializedProperty caretColorProperty;
		private SerializedProperty selectionColorProperty;
		private SerializedProperty readOnlyProperty;
		private SerializedProperty scrollBehaviourOnEndEditProperty;
		private SerializedProperty scrollBarsVisibilityModeProperty;
		private SerializedProperty scrollSpeedProperty;
		private SerializedProperty fastScrollSensitivityProperty;
		private SerializedProperty resizeMinWidthProperty;
		private SerializedProperty resizeMinHeightProperty;
		private SerializedProperty onSelectionChangedProperty;
		private SerializedProperty onBeginEditProperty;
		private SerializedProperty onEndEditProperty;
		private SerializedProperty onValueChangedProperty;
		private SerializedProperty onCaretPositionChangedProperty;
		private SerializedProperty onTextSelectionChangedProperty;
		private SerializedProperty onSizeChangedProperty;
		private SerializedProperty onSpecialKeyPressedProperty;
		private SerializedProperty actionBarProperty;
		private SerializedProperty actionBarCutProperty;
		private SerializedProperty actionBarCopyProperty;
		private SerializedProperty actionBarPasteProperty;
		private SerializedProperty actionBarSelectAllProperty;
		private SerializedProperty selectionCursorsProperty;
		private SerializedProperty cursorClampModeProperty;
		private SerializedProperty autocapitalizationTypeProperty;
		private SerializedProperty autofillTypeProperty;
		private SerializedProperty returnKeyTypeProperty;
		private SerializedProperty nextInputFieldProperty;

		private void OnEnable()
		{
			interactableProperty = serializedObject.FindProperty("m_Interactable");
			targetGraphicProperty = serializedObject.FindProperty("m_TargetGraphic");
			transitionProperty = serializedObject.FindProperty("m_Transition");
			colorBlockProperty = serializedObject.FindProperty("m_Colors");
			spriteStateProperty = serializedObject.FindProperty("m_SpriteState");
			animTriggerProperty = serializedObject.FindProperty("m_AnimationTriggers");
			showColorTint.valueChanged.AddListener(Repaint);
			showSpriteTrasition.valueChanged.AddListener(Repaint);

			modeProperty = serializedObject.FindProperty("mode");
			textProperty = serializedObject.FindProperty("text");
			placeholderTextProperty = serializedObject.FindProperty("placeholderText");
			richTextEditingProperty = serializedObject.FindProperty("richTextEditing");
			richTextConfigProperty = serializedObject.FindProperty("richTextConfig");
			characterLimitProperty = serializedObject.FindProperty("characterLimit");
			lineLimitProperty = serializedObject.FindProperty("lineLimit");
			contentTypeProperty = serializedObject.FindProperty("contentType");
			lineTypeProperty = serializedObject.FindProperty("lineType");
			inputTypeProperty = serializedObject.FindProperty("inputType");
			visiblePasswordProperty = serializedObject.FindProperty("visiblePassword");
			keyboardTypeProperty = serializedObject.FindProperty("keyboardType");
			characterValidationProperty = serializedObject.FindProperty("characterValidation");
			characterValidatorProperty = serializedObject.FindProperty("characterValidator");
			emojisAllowedProperty = serializedObject.FindProperty("emojisAllowed");
			liveProcessingFilterProperty = serializedObject.FindProperty("liveProcessingFilter");
			postProcessingFilterProperty = serializedObject.FindProperty("postProcessingFilter");
			selectionModeProperty = serializedObject.FindProperty("selectionMode");
			dragModeProperty = serializedObject.FindProperty("dragMode");
			caretOnBeginEditProperty = serializedObject.FindProperty("caretOnBeginEdit");
			caretBlinkRateProperty = serializedObject.FindProperty("caretBlinkRate");
			caretWidthProperty = serializedObject.FindProperty("caretWidth");
			caretColorProperty = serializedObject.FindProperty("caretColor");
			selectionColorProperty = serializedObject.FindProperty("selectionColor");
			readOnlyProperty = serializedObject.FindProperty("readOnly");
			scrollBehaviourOnEndEditProperty = serializedObject.FindProperty("scrollBehaviourOnEndEdit");
			scrollBarsVisibilityModeProperty = serializedObject.FindProperty("scrollBarsVisibilityMode");
			scrollSpeedProperty = serializedObject.FindProperty("scrollSpeed");
			fastScrollSensitivityProperty = serializedObject.FindProperty("fastScrollSensitivity");
			resizeMinWidthProperty = serializedObject.FindProperty("resizeMinWidth");
			resizeMinHeightProperty = serializedObject.FindProperty("resizeMinHeight");
			onSelectionChangedProperty = serializedObject.FindProperty("onSelectionChanged");
			onBeginEditProperty = serializedObject.FindProperty("onBeginEdit");
			onEndEditProperty = serializedObject.FindProperty("onEndEdit");
			onValueChangedProperty = serializedObject.FindProperty("onValueChanged");
			onCaretPositionChangedProperty = serializedObject.FindProperty("onCaretPositionChanged");
			onTextSelectionChangedProperty = serializedObject.FindProperty("onTextSelectionChanged");
			onSizeChangedProperty = serializedObject.FindProperty("onSizeChanged");
			onSpecialKeyPressedProperty = serializedObject.FindProperty("onSpecialKeyPressed");
			actionBarProperty = serializedObject.FindProperty("actionBar");
			actionBarCutProperty = serializedObject.FindProperty("actionBarCut");
			actionBarCopyProperty = serializedObject.FindProperty("actionBarCopy");
			actionBarPasteProperty = serializedObject.FindProperty("actionBarPaste");
			actionBarSelectAllProperty = serializedObject.FindProperty("actionBarSelectAll");
			selectionCursorsProperty = serializedObject.FindProperty("selectionCursors");
			cursorClampModeProperty = serializedObject.FindProperty("cursorClampMode");
			autocapitalizationTypeProperty = serializedObject.FindProperty("autocapitalizationType");
			autofillTypeProperty = serializedObject.FindProperty("autofillType");
			returnKeyTypeProperty = serializedObject.FindProperty("returnKeyType");
			nextInputFieldProperty = serializedObject.FindProperty("nextInputField");
		}
		private void OnDisable()
		{
			showColorTint.valueChanged.RemoveListener(Repaint);
			showSpriteTrasition.valueChanged.RemoveListener(Repaint);
		}

		public override void OnInspectorGUI()
		{
			serializedObject.Update();

			AdvancedInputField inputField = (AdvancedInputField)target;

			DrawHeader(inputField);
			DrawInheritedProperties();
			InputFieldMode mode = DrawModeProperty(inputField);

			DrawTextProperties(inputField);
			DrawCharacterLimitProperty(inputField);
			DrawLineLimitProperty(inputField);
			DrawContentTypeProperties(inputField);

			EditorGUILayout.PropertyField(liveProcessingFilterProperty);
			EditorGUILayout.PropertyField(postProcessingFilterProperty);
			EditorGUILayout.PropertyField(selectionModeProperty);
			EditorGUILayout.PropertyField(dragModeProperty);
			DrawCaretProperties();

			EditorGUILayout.PropertyField(selectionColorProperty);
			EditorGUILayout.PropertyField(readOnlyProperty);
			EditorGUILayout.PropertyField(nextInputFieldProperty);

			if(mode == InputFieldMode.SCROLL_TEXT)
			{
				DrawTextScrollProperties(inputField);
			}
			else if(mode == InputFieldMode.HORIZONTAL_RESIZE_FIT_TEXT)
			{
				DrawResizeHorizontalProperties(inputField);
			}
			else if(mode == InputFieldMode.VERTICAL_RESIZE_FIT_TEXT)
			{
				DrawResizeVerticalProperties(inputField);
			}

			DrawEventProperties();
			DrawActionBarProperties();
			DrawMobileOnlyProperties();

			serializedObject.ApplyModifiedProperties();
		}

		private void DrawHeader(AdvancedInputField inputField)
		{
			if(inputField.GetComponentInChildren<ScrollArea>() != null) //New format
			{
#if ADVANCEDINPUTFIELD_TEXTMESHPRO
				if(inputField.GetComponentInChildren<TMPro.TextMeshProUGUI>() != null)
				{
					Texture2D texture = Resources.Load<Texture2D>(TEXTMESHPRO_FORMAT_IMAGE_PATH);
					GUILayout.Box(texture);
				}
				else
#endif
				{
					Texture2D texture = Resources.Load<Texture2D>(UNITY_FORMAT_IMAGE_PATH);
					GUILayout.Box(texture);
				}
			}
			else //Old format
			{
				Texture2D texture = Resources.Load<Texture2D>(DEPRECATED_FORMAT_RESOURCE_PATH);
				GUILayout.Box(texture);
				GUIStyle labelStyle = new GUIStyle(EditorStyles.label);
				labelStyle.normal.textColor = new Color(0.5f, 0, 0);
				labelStyle.fontSize = 12;
				string message = "This format is deprecated, please use the ConversionTool to convert the InputField(s) to the newer format." +
					"\n(TopBar: Advanced Input Field => ConversionTool)" +
					 "\nSet \'from\' to \'DEPRECATED_ADVANCEDINPUTFIELD\' and \'to\' to either \'ADVANCEDINPUTFIELD_UNITY_TEXT\' or \'ADVANCEDINPUTFIELD_TEXTMESHPRO_TEXT\'";
				GUILayout.Label(message, labelStyle);
				EditorGUILayout.Space();
			}
		}

		#region INHERITED
		private void DrawInheritedProperties()
		{
			EditorGUILayout.PropertyField(interactableProperty);
			var trans = GetTransition(transitionProperty);

			var graphic = targetGraphicProperty.objectReferenceValue as Graphic;
			if(graphic == null)
			{
				graphic = (target as Selectable).GetComponentInChildren<Graphic>();
			}

			var animator = (target as Selectable).GetComponent<Animator>();
			showColorTint.target = (!transitionProperty.hasMultipleDifferentValues && trans == Button.Transition.ColorTint);
			showSpriteTrasition.target = (!transitionProperty.hasMultipleDifferentValues && trans == Button.Transition.SpriteSwap);
			showAnimTransition.target = (!transitionProperty.hasMultipleDifferentValues && trans == Button.Transition.Animation);

			EditorGUILayout.PropertyField(transitionProperty);

			++EditorGUI.indentLevel;
			{
				if(trans == Selectable.Transition.ColorTint || trans == Selectable.Transition.SpriteSwap)
				{
					EditorGUILayout.PropertyField(targetGraphicProperty);
				}

				switch(trans)
				{
					case Selectable.Transition.ColorTint:
						if(graphic == null)
						{
							EditorGUILayout.HelpBox("You must have a Graphic target in order to use a color transition.", MessageType.Warning);
						}
						break;

					case Selectable.Transition.SpriteSwap:
						if(graphic as Image == null)
						{
							EditorGUILayout.HelpBox("You must have a Image target in order to use a sprite swap transition.", MessageType.Warning);
						}
						break;
				}

				if(EditorGUILayout.BeginFadeGroup(showColorTint.faded))
				{
					EditorGUILayout.PropertyField(colorBlockProperty);
				}
				EditorGUILayout.EndFadeGroup();

				if(EditorGUILayout.BeginFadeGroup(showSpriteTrasition.faded))
				{
					EditorGUILayout.PropertyField(spriteStateProperty);
				}
				EditorGUILayout.EndFadeGroup();

				if(EditorGUILayout.BeginFadeGroup(showAnimTransition.faded))
				{
					EditorGUILayout.PropertyField(animTriggerProperty);

					if(animator == null || animator.runtimeAnimatorController == null)
					{
						Rect buttonRect = EditorGUILayout.GetControlRect();
						buttonRect.xMin += EditorGUIUtility.labelWidth;
						if(GUI.Button(buttonRect, "Auto Generate Animation", EditorStyles.miniButton))
						{
							var controller = GenerateSelectableAnimatorContoller((target as Selectable).animationTriggers, target as Selectable);
							if(controller != null)
							{
								if(animator == null)
								{
									animator = (target as Selectable).gameObject.AddComponent<Animator>();
								}

								UnityEditor.Animations.AnimatorController.SetAnimatorController(animator, controller);
							}
						}
					}
				}
				EditorGUILayout.EndFadeGroup();
			}
			--EditorGUI.indentLevel;

			EditorGUILayout.Space();
		}

		static Selectable.Transition GetTransition(SerializedProperty transition)
		{
			return (Selectable.Transition)transition.enumValueIndex;
		}

		private static UnityEditor.Animations.AnimatorController GenerateSelectableAnimatorContoller(AnimationTriggers animationTriggers, Selectable target)
		{
			if(target == null)
			{
				return null;
			}

			// Where should we create the controller?
			var path = GetSaveControllerPath(target);
			if(string.IsNullOrEmpty(path))
			{
				return null;
			}

			// figure out clip names
			var normalName = string.IsNullOrEmpty(animationTriggers.normalTrigger) ? "Normal" : animationTriggers.normalTrigger;
			var highlightedName = string.IsNullOrEmpty(animationTriggers.highlightedTrigger) ? "Highlighted" : animationTriggers.highlightedTrigger;
			var pressedName = string.IsNullOrEmpty(animationTriggers.pressedTrigger) ? "Pressed" : animationTriggers.pressedTrigger;
			var disabledName = string.IsNullOrEmpty(animationTriggers.disabledTrigger) ? "Disabled" : animationTriggers.disabledTrigger;

			// Create controller and hook up transitions.
			var controller = UnityEditor.Animations.AnimatorController.CreateAnimatorControllerAtPath(path);
			GenerateTriggerableTransition(normalName, controller);
			GenerateTriggerableTransition(highlightedName, controller);
			GenerateTriggerableTransition(pressedName, controller);
			GenerateTriggerableTransition(disabledName, controller);

			AssetDatabase.ImportAsset(path);

			return controller;
		}

		private static string GetSaveControllerPath(Selectable target)
		{
			var defaultName = target.gameObject.name;
			var message = string.Format("Create a new animator for the game object '{0}':", defaultName);
			return EditorUtility.SaveFilePanelInProject("New Animation Contoller", defaultName, "controller", message);
		}

		private static void SetUpCurves(AnimationClip highlightedClip, AnimationClip pressedClip, string animationPath)
		{
			string[] channels = { "m_LocalScale.x", "m_LocalScale.y", "m_LocalScale.z" };

			var highlightedKeys = new[] { new Keyframe(0f, 1f), new Keyframe(0.5f, 1.1f), new Keyframe(1f, 1f) };
			var highlightedCurve = new AnimationCurve(highlightedKeys);
			foreach(var channel in channels)
			{
				AnimationUtility.SetEditorCurve(highlightedClip, EditorCurveBinding.FloatCurve(animationPath, typeof(Transform), channel), highlightedCurve);
			}

			var pressedKeys = new[] { new Keyframe(0f, 1.15f) };
			var pressedCurve = new AnimationCurve(pressedKeys);
			foreach(var channel in channels)
			{
				AnimationUtility.SetEditorCurve(pressedClip, EditorCurveBinding.FloatCurve(animationPath, typeof(Transform), channel), pressedCurve);
			}
		}

		private static string BuildAnimationPath(Selectable target)
		{
			// if no target don't hook up any curves.
			var highlight = target.targetGraphic;
			if(highlight == null)
			{
				return string.Empty;
			}

			var startGo = highlight.gameObject;
			var toFindGo = target.gameObject;

			var pathComponents = new Stack<string>();
			while(toFindGo != startGo)
			{
				pathComponents.Push(startGo.name);

				// didn't exist in hierarchy!
				if(startGo.transform.parent == null)
				{
					return string.Empty;
				}

				startGo = startGo.transform.parent.gameObject;
			}

			// calculate path
			var animPath = new StringBuilder();
			if(pathComponents.Count > 0)
			{
				animPath.Append(pathComponents.Pop());
			}

			while(pathComponents.Count > 0)
			{
				animPath.Append("/").Append(pathComponents.Pop());
			}

			return animPath.ToString();
		}

		private static AnimationClip GenerateTriggerableTransition(string name, UnityEditor.Animations.AnimatorController controller)
		{
			// Create the clip
			var clip = UnityEditor.Animations.AnimatorController.AllocateAnimatorClip(name);
			AssetDatabase.AddObjectToAsset(clip, controller);

			// Create a state in the animatior controller for this clip
			var state = controller.AddMotion(clip);

			// Add a transition property
			controller.AddParameter(name, AnimatorControllerParameterType.Trigger);

			// Add an any state transition
			var stateMachine = controller.layers[0].stateMachine;
			var transition = stateMachine.AddAnyStateTransition(state);
			transition.AddCondition(UnityEditor.Animations.AnimatorConditionMode.If, 0, name);
			return clip;
		}
		#endregion

		private InputFieldMode DrawModeProperty(AdvancedInputField inputField)
		{
			InputFieldMode mode = (InputFieldMode)modeProperty.enumValueIndex;

			EditorGUI.BeginChangeCheck();
			EditorGUILayout.PropertyField(modeProperty);
			if(EditorGUI.EndChangeCheck())
			{
				mode = (InputFieldMode)modeProperty.enumValueIndex;
				inputField.Mode = mode;
			}

			return mode;
		}

		private void DrawTextProperties(AdvancedInputField inputField)
		{
			EditorGUI.BeginChangeCheck();
			EditorGUILayout.PropertyField(textProperty);
			if(EditorGUI.EndChangeCheck())
			{
				if(string.IsNullOrEmpty(textProperty.stringValue))
				{
					MarkTextRendererDirty(inputField.PlaceholderTextRenderer);
				}
				else
				{
					MarkTextRendererDirty(inputField.TextRenderer);
				}

				inputField.Text = textProperty.stringValue;
			}

			EditorGUI.BeginChangeCheck();
			EditorGUILayout.PropertyField(placeholderTextProperty);
			if(EditorGUI.EndChangeCheck())
			{
				MarkTextRendererDirty(inputField.PlaceholderTextRenderer);
				inputField.PlaceHolderText = placeholderTextProperty.stringValue;
			}

			EditorGUILayout.PropertyField(richTextEditingProperty);
			if(richTextEditingProperty.boolValue)
			{
				EditorGUILayout.PropertyField(richTextConfigProperty);
			}
		}

		private void MarkTextRendererDirty(TextRenderer textRenderer)
		{
			if(textRenderer is UnityTextRenderer)
			{
				Undo.RecordObject(((UnityTextRenderer)textRenderer).Renderer, "Undo " + textRenderer.GetInstanceID());
			}
#if ADVANCEDINPUTFIELD_TEXTMESHPRO
			else if(textRenderer is TMProTextRenderer)
			{
				Undo.RecordObject(((TMProTextRenderer)textRenderer).Renderer, "Undo " + textRenderer.GetInstanceID());
			}
#endif
		}

		private void DrawCharacterLimitProperty(AdvancedInputField inputField)
		{
			EditorGUI.BeginChangeCheck();
			EditorGUILayout.PropertyField(characterLimitProperty);
			if(EditorGUI.EndChangeCheck())
			{
				inputField.ApplyCharacterLimit(characterLimitProperty.intValue);
				textProperty.stringValue = inputField.Text;
			}
		}

		private void DrawLineLimitProperty(AdvancedInputField inputField) //TODO: Reenable when line limit works on mobile too (requires partial rewrite native bindings)
		{
			EditorGUI.BeginChangeCheck();
			EditorGUILayout.PropertyField(lineLimitProperty);
			if(EditorGUI.EndChangeCheck())
			{
				inputField.ApplyLineLimit(lineLimitProperty.intValue);
				textProperty.stringValue = inputField.Text;
			}
		}

		private void DrawContentTypeProperties(AdvancedInputField inputField)
		{
			EditorGUI.BeginChangeCheck();
			EditorGUILayout.PropertyField(contentTypeProperty);
			if(EditorGUI.EndChangeCheck())
			{
				ContentType contentType = ((ContentType)contentTypeProperty.enumValueIndex);
				inputField.UpdateContentType(contentType);
				lineTypeProperty.enumValueIndex = (int)inputField.LineType;
				inputTypeProperty.enumValueIndex = (int)inputField.InputType;
				keyboardTypeProperty.enumValueIndex = (int)inputField.KeyboardType;
				characterValidationProperty.enumValueIndex = (int)inputField.CharacterValidation;
				emojisAllowedProperty.boolValue = inputField.EmojisAllowed;
			}

			EditorGUI.indentLevel = 1;
			EditorGUILayout.PropertyField(lineTypeProperty);
			if(((ContentType)contentTypeProperty.enumValueIndex) == ContentType.Custom)
			{
				EditorGUILayout.PropertyField(inputTypeProperty);
				if(((InputType)inputTypeProperty.enumValueIndex) == InputType.Password)
				{
					EditorGUILayout.PropertyField(visiblePasswordProperty);
				}

				EditorGUILayout.PropertyField(keyboardTypeProperty);
				EditorGUILayout.PropertyField(characterValidationProperty);
				if(((CharacterValidation)characterValidationProperty.enumValueIndex) == CharacterValidation.Custom)
				{
					EditorGUILayout.PropertyField(characterValidatorProperty);
				}

				EditorGUILayout.PropertyField(emojisAllowedProperty, new GUIContent("Emojis Allowed"));
			}
			EditorGUI.indentLevel = 0;
		}

		private void DrawCaretProperties()
		{
			EditorGUILayout.PropertyField(caretOnBeginEditProperty);
			EditorGUILayout.PropertyField(caretBlinkRateProperty);
			EditorGUILayout.PropertyField(caretWidthProperty);
			EditorGUILayout.PropertyField(caretColorProperty);
		}

		private void DrawTextScrollProperties(AdvancedInputField inputField)
		{
			EditorGUILayout.Space();
			EditorGUILayout.LabelField("Text scroll:", EditorStyles.boldLabel);
			EditorGUILayout.PropertyField(scrollBehaviourOnEndEditProperty);

			EditorGUI.BeginChangeCheck();
			EditorGUILayout.PropertyField(scrollBarsVisibilityModeProperty);
			if(EditorGUI.EndChangeCheck())
			{
				ScrollArea scrollArea = inputField.TextAreaTransform.GetComponent<ScrollArea>();
				Undo.RecordObject(scrollArea, "Undo " + scrollArea.GetInstanceID());
				inputField.ScrollBarsVisibilityMode = (ScrollbarVisibilityMode)scrollBarsVisibilityModeProperty.enumValueIndex;
			}

			EditorGUILayout.PropertyField(scrollSpeedProperty);
			EditorGUILayout.PropertyField(fastScrollSensitivityProperty);
		}

		private void DrawResizeHorizontalProperties(AdvancedInputField inputField)
		{
			EditorGUILayout.Space();
			EditorGUILayout.LabelField("Resize Horizontal:", EditorStyles.boldLabel);
			EditorGUILayout.PropertyField(resizeMinWidthProperty);
		}

		private void DrawResizeVerticalProperties(AdvancedInputField inputField)
		{
			EditorGUILayout.Space();
			EditorGUILayout.LabelField("Resize Vertical:", EditorStyles.boldLabel);
			EditorGUILayout.PropertyField(resizeMinHeightProperty);
		}

		private void DrawEventProperties()
		{
			EditorGUILayout.Space();
			EditorGUILayout.LabelField("Events:", EditorStyles.boldLabel);
			EditorGUILayout.PropertyField(onSelectionChangedProperty);
			EditorGUILayout.PropertyField(onBeginEditProperty);
			EditorGUILayout.PropertyField(onEndEditProperty);
			EditorGUILayout.PropertyField(onValueChangedProperty);
			EditorGUILayout.PropertyField(onCaretPositionChangedProperty);
			EditorGUILayout.PropertyField(onTextSelectionChangedProperty);
			EditorGUILayout.PropertyField(onSizeChangedProperty);
			EditorGUILayout.PropertyField(onSpecialKeyPressedProperty);
		}

		private void DrawActionBarProperties()
		{
			EditorGUILayout.Space();
			EditorGUILayout.LabelField("ActionBar:", EditorStyles.boldLabel);
			EditorGUILayout.PropertyField(actionBarProperty);
			if(actionBarProperty.boolValue)
			{
				EditorGUILayout.PropertyField(actionBarCutProperty);
				EditorGUILayout.PropertyField(actionBarCopyProperty);
				EditorGUILayout.PropertyField(actionBarPasteProperty);
				EditorGUILayout.PropertyField(actionBarSelectAllProperty);
			}
		}

		private void DrawMobileOnlyProperties()
		{
			EditorGUILayout.Space();
			EditorGUILayout.LabelField("Mobile only:", EditorStyles.boldLabel);
			EditorGUILayout.PropertyField(selectionCursorsProperty);
			if(selectionCursorsProperty.boolValue)
			{
				EditorGUILayout.PropertyField(cursorClampModeProperty);
			}
			EditorGUILayout.PropertyField(autocapitalizationTypeProperty);
			EditorGUILayout.PropertyField(autofillTypeProperty);
			EditorGUILayout.PropertyField(returnKeyTypeProperty);
		}
	}
}