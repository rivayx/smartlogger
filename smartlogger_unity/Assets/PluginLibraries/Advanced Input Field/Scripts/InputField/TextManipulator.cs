﻿//-----------------------------------------
//			Advanced Input Field
// Copyright (c) 2017 Jeroen van Pienbroek
//------------------------------------------

using UnityEngine;

namespace AdvancedInputFieldPlugin
{
	/// <summary>Base class for text string manipulation</summary>
	public class TextManipulator
	{
		/// <summary>The TextValidator</summary>
		private TextValidator textValidator;

		/// <summary>The InputField</summary>
		public AdvancedInputField InputField { get; private set; }

		/// <summary>The TextNavigator/summary>
		public virtual TextNavigator TextNavigator { get; protected set; }

		/// <summary>The main renderer for text</summary>
		public TextRenderer TextRenderer { get; private set; }

		/// <summary>The renderer for processed text</summary>
		public TextRenderer ProcessedTextRenderer { get; private set; }

		/// <summary>The main text string</summary>
		public virtual string Text
		{
			get { return InputField.Text; }
			set
			{
				InputField.SetText(value, true);
			}
		}

		/// <summary>The text in the clipboard</summary>
		public static string Clipboard
		{
			get
			{
				return GUIUtility.systemCopyBuffer;
			}
			set
			{
				GUIUtility.systemCopyBuffer = value;
			}
		}

		/// <summary>Indicates whether to currrently block text change events from being send to the native bindings</summary>
		public bool BlockNativeTextChange { get; set; }

		/// <summary>Initializes the class</summary>
		internal virtual void Initialize(AdvancedInputField inputField, TextNavigator textNavigator, TextRenderer textRenderer, TextRenderer processedTextRenderer)
		{
			InputField = inputField;
			TextNavigator = textNavigator;
			TextRenderer = textRenderer;
			ProcessedTextRenderer = processedTextRenderer;
			textValidator = new TextValidator(InputField.CharacterValidation, InputField.LineType, inputField.CharacterValidator);
		}

		internal void RefreshTextValidator()
		{
			textValidator = new TextValidator(InputField.CharacterValidation, InputField.LineType, InputField.CharacterValidator);
		}

		/// <summary>Begins the edit mode</summary>
		internal void BeginEditMode()
		{
			TextNavigator.RefreshRenderedText();
			textValidator.Validator = InputField.CharacterValidator;
			if(InputField.LiveProcessing)
			{
				LiveProcessingFilter liveProcessingFilter = InputField.LiveProcessingFilter;
				string text = InputField.Text;
				int caretPosition = TextNavigator.CaretPosition;
				string processedText = liveProcessingFilter.ProcessText(text, caretPosition);
				if(processedText != null)
				{
					InputField.ProcessedText = processedText;

					int processedCaretPosition = liveProcessingFilter.DetermineProcessedCaret(text, caretPosition, processedText);
					TextNavigator.ProcessedCaretPosition = processedCaretPosition;
				}
			}
		}

		/// <summary>Ends the edit mode</summary>
		internal void EndEditMode()
		{
			if(InputField.PostProcessingFilter != null)
			{
				string processedText = null;
				if(string.IsNullOrEmpty(Text)) //Empty
				{
					InputField.ProcessedText = string.Empty;
				}
				else if(InputField.PostProcessingFilter.ProcessText(Text, out processedText)) //Try to process text
				{
					InputField.ProcessedText = processedText;
				}
				else //Couldn't be parsed, use original value
				{
					InputField.ProcessedText = Text;
				}
			}
		}

		/// <summary>Checks if character is valid</summary>
		/// <param name="c">The character to check</param>
		internal bool IsValidChar(char c)
		{
			if((int)c == 127) //Delete key on mac
			{
				return false;
			}

			if(c == '\t' || c == '\n') // Accept newline and tab
			{
				return true;
			}

			return TextRenderer.FontHasCharacter(c);
		}

		/// <summary>Insert a string at caret position</summary>
		/// <param name="input">the string to insert</param>
		internal virtual void Insert(string input)
		{
			if(InputField.ReadOnly)
			{
				return;
			}

			if(TextNavigator.HasSelection)
			{
				DeleteSelection();
			}

			if(InputField.CharacterLimit > 0 && Text.Length + input.Length > InputField.CharacterLimit)
			{
				if(Text.Length < InputField.CharacterLimit)
				{
					int amountAllowed = InputField.CharacterLimit - Text.Length;
					input = input.Substring(0, amountAllowed);
				}
				else
				{
					return;
				}
			}

			string lastText = Text;

			int selectionStartPosition = -1;
			if(TextNavigator.HasSelection)
			{
				selectionStartPosition = TextNavigator.SelectionStartPosition;
			}
			textValidator.Validate(Text, input, TextNavigator.CaretPosition, selectionStartPosition);
			string resultText = textValidator.ResultText;
			int resultCaretPosition = textValidator.ResultCaretPosition;

			ApplyCharacterLimit(ref resultText, ref resultCaretPosition);
			TextRenderer.Text = resultText;

			if(InputField.LineLimit > 0 && TextRenderer.LineCount > InputField.LineLimit)
			{
				if(input.Length > 1)
				{
					input = input.Substring(0, input.Length - 1);
					Insert(input); //Try again to stay within line limit
				}
				else
				{
					TextRenderer.Text = lastText;
				}
				return;
			}

			TextEditFrame textEditFrame = new TextEditFrame(resultText, resultCaretPosition, resultCaretPosition, resultCaretPosition);
			InputField.ApplyTextEditFrame(textEditFrame);
		}

		public void ApplyCharacterLimit(ref string text, ref int caretPosition)
		{
			if(InputField.CharacterLimit != 0 && text.Length > InputField.CharacterLimit)
			{
				int amountOverLimit = text.Length - InputField.CharacterLimit;
				text = text.Substring(0, InputField.CharacterLimit);
				caretPosition = Mathf.Clamp(caretPosition, 0, Text.Length);
			}
		}

		/// <summary>Tries to insert text</summary>
		/// <param name="text">The text to insert</param>
		internal void TryInsertText(string text)
		{
			if(InputField.ReadOnly)
			{
				return;
			}

			Insert(text);
		}

		/// <summary>Tries to insert a character</summary>
		/// <param name="c">The character to insert</param>
		internal void TryInsertChar(char c)
		{
			if(!IsValidChar(c))
			{
				return;
			}

			Insert(c.ToString());
		}

		/// <summary>Deletes previous character</summary>
		internal void DeletePreviousChar()
		{
			if(InputField.ReadOnly)
			{
				return;
			}

			int originalLineCount = TextNavigator.LineCount;

			if(TextNavigator.HasSelection)
			{
				DeleteSelection();
			}
			else if(TextNavigator.CaretPosition > 0)
			{
				if(InputField.EmojisAllowed)
				{
					TextRenderer activeTextRenderer = InputField.GetActiveTextRenderer();
					int caretPosition = TextNavigator.CaretPosition;
					CharacterInfo characterInfo = activeTextRenderer.GetCharacterInfo(caretPosition);
					int currentIndex = characterInfo.index; //This will always be the startIndex if surrogate pair
					if(currentIndex > 0)
					{
						CharacterInfo previousCharacterInfo = activeTextRenderer.GetCharacterInfo(currentIndex - 1);
						int amount = currentIndex - previousCharacterInfo.index;
						caretPosition -= amount;
						string text = Text.Remove(caretPosition, amount);
						TextEditFrame textEditFrame = new TextEditFrame(text, caretPosition, 0, 0);
						InputField.ApplyTextEditFrame(textEditFrame);
					}

				}
				else
				{
					int caretPosition = TextNavigator.CaretPosition - 1;
					string text = Text.Remove(caretPosition, 1);
					TextEditFrame textEditFrame = new TextEditFrame(text, caretPosition, 0, 0);
					InputField.ApplyTextEditFrame(textEditFrame);
				}
			}
		}

		/// <summary>Deletes next character</summary>
		internal void DeleteNextChar()
		{
			if(InputField.ReadOnly)
			{
				return;
			}

			int originalLineCount = TextNavigator.LineCount;

			if(TextNavigator.HasSelection)
			{
				DeleteSelection();
			}
			else if(TextNavigator.CaretPosition < Text.Length)
			{
				if(InputField.EmojisAllowed)
				{
					TextRenderer activeTextRenderer = InputField.GetActiveTextRenderer();
					int caretPosition = TextNavigator.CaretPosition;
					CharacterInfo characterInfo = activeTextRenderer.GetCharacterInfo(caretPosition);
					int currentIndex = characterInfo.index; //This will always be the startIndex if surrogate pair
					if(currentIndex < Text.Length)
					{
						int amount = characterInfo.partCount;
						caretPosition = currentIndex;
						string text = Text.Remove(caretPosition, amount);
						TextEditFrame textEditFrame = new TextEditFrame(text, caretPosition, 0, 0);
						InputField.ApplyTextEditFrame(textEditFrame);
					}
				}
				else
				{
					int caretPosition = TextNavigator.CaretPosition;
					string text = Text.Remove(caretPosition, 1);
					TextEditFrame textEditFrame = new TextEditFrame(text, caretPosition, 0, 0);
					InputField.ApplyTextEditFrame(textEditFrame);
				}
			}
		}

		/// <summary>Deletes current text selection</summary>
		internal virtual void DeleteSelection()
		{
			if(InputField.ReadOnly)
			{
				return;
			}

			string text = Text.Remove(TextNavigator.SelectionStartPosition, TextNavigator.SelectionEndPosition - TextNavigator.SelectionStartPosition);
			int caretPosition = TextNavigator.SelectionStartPosition;
			TextEditFrame textEditFrame = new TextEditFrame(text, caretPosition, 0, 0);
			InputField.ApplyTextEditFrame(textEditFrame);

			InputField.ResetDragStartPosition(TextNavigator.CaretPosition);
		}

		/// <summary>Copies current text selection</summary>
		internal virtual void Copy()
		{
			if(!InputField.IsPasswordField)
			{
				Clipboard = TextNavigator.SelectedText;
			}
			else
			{
				Clipboard = string.Empty;
			}
		}

		/// <summary>Pastes clipboard text</summary>
		internal virtual void Paste()
		{
			if(InputField.ReadOnly)
			{
				return;
			}

			string input = Clipboard;
			string processedInput = string.Empty;

			int length = input.Length;
			for(int i = 0; i < length; i++)
			{
				char c = input[i];

				if(c >= ' ' || c == '\t' || c == '\r' || c == 10 || c == '\n')
				{
					processedInput += c;
				}
			}

			if(!string.IsNullOrEmpty(processedInput))
			{
				Insert(processedInput);
			}
		}

		/// <summary>Cuts current text selection</summary>
		internal virtual void Cut()
		{
			if(!InputField.IsPasswordField)
			{
				Clipboard = TextNavigator.SelectedText;
			}
			else
			{
				Clipboard = string.Empty;
			}

			if(InputField.ReadOnly)
			{
				return;
			}

			if(TextNavigator.HasSelection)
			{
				DeleteSelection();
			}
		}

		/// <summary>Replaces current word with given text</summary>
		internal virtual void Replace(string text)
		{
			if(InputField.ReadOnly)
			{
				return;
			}

			TextRange range;
			if(TextNavigator.HasSelection)
			{
				Insert(text);
			}
			else if(TryGetWordRange(InputField.Text, TextNavigator.CaretPosition, out range))
			{
				InputField.SetTextSelection(range.start, range.end + 1);
				Insert(text);
			}
		}

		public bool TryGetWordRange(string text, int position, out TextRange range)
		{
			if(position >= text.Length || !char.IsLetter(text[position])) //Not in a word
			{
				range = default(TextRange);
				return false;
			}

			int wordStart = position;
			int wordEnd = position;

			int length = text.Length;
			for(int i = position - 1; i >= 0; i--)
			{
				if(!char.IsLetter(text[i]))
				{
					break;
				}

				wordStart = i;
			}
			for(int i = position + 1; i < length; i++)
			{
				if(!char.IsLetter(text[i]))
				{
					break;
				}

				wordEnd = i;
			}

			if(wordEnd > wordStart)
			{
				range = new TextRange(wordStart, wordEnd);
				return true;
			}
			else
			{
				range = default(TextRange);
				return false;
			}
		}
	}
}
