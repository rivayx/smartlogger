﻿using UnityEngine;
using UnityEngine.UI;

namespace AdvancedInputFieldPlugin
{
	public delegate void OnActionBarItemClick(ActionBarItem item);

	public class ActionBarItem: MonoBehaviour
	{
		private Text label;
		private event OnActionBarItemClick onClick;
		private bool initialized;

		public ActionBarAction Action { get; private set; }

		public event OnActionBarItemClick Click
		{
			add { onClick += value; }
			remove { onClick -= value; }
		}

		private void Awake()
		{
			if(!initialized) { Initialize(); }
		}

		private void Initialize()
		{
			label = GetComponentInChildren<Text>();
			initialized = true;
		}

		public void ConfigureUI(ActionBarAction action)
		{
			if(!initialized) { Initialize(); }

			Action = action;
			label.text = action.text;
		}

		public void OnClick()
		{
			onClick?.Invoke(this);
		}
	}
}
