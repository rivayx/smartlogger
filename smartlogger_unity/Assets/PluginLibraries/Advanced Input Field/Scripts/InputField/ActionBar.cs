﻿//-----------------------------------------
//			Advanced Input Field
// Copyright (c) 2017 Jeroen van Pienbroek
//------------------------------------------

using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace AdvancedInputFieldPlugin
{
	public enum ActionBarActionType { CUT, COPY, PASTE, SELECT_ALL, SHOW_REPLACE, REPLACE, CUSTOM }

	/// <summary>The onscreen control for cut, copy, paste and select all operations</summary>
	[RequireComponent(typeof(RectTransform))]
	public class ActionBar: MonoBehaviour
	{
		/// <summary>The multiplier for thumb height to calculate action bar height</summary>
		private const float THUMB_SIZE_RATIO = 0.4f;

		/// <summary>The multiplier of canvas min size (width or height) to calculate action bar width</summary>
		private const float CANVAS_MIN_SIZE_RATIO = 0.9f;

		/// <summary>The maximum amount of buttons that can active at one time (cut, copy, paste, select all)</summary>
		private const int MAX_ACTIONS_PER_PAGE = 3;

		private const int MAX_ACTIONS_IN_BAR = 4;

		/// <summary>The RectTransform for the ActionBar that will be rendered last in the Canvas</summary>
		[SerializeField]
		private CanvasFrontRenderer actionBarRenderer;

		[SerializeField]
		private Button previousPageButton;

		[SerializeField]
		private Button nextPageButton;

		[SerializeField]
		private bool showDividers = true;

		[SerializeField]
		private bool defaultActionsFirst = true;

		[SerializeField]
		private bool showSoloReplaceActionImmediately = false;

		private ActionBarItem[] itemPool;
		private Image[] dividerPool;

		/// <summary>The RectTransform</summary>
		public RectTransform RectTransform { get; private set; }

		/// <summary>The max size of the ActionBar when all buttons are enabled</summary>
		private Vector2 fullSize;

		/// <summary>The size of a button</summary>
		private Vector2 buttonSize;

		/// <summary>The InputField</summary>
		public AdvancedInputField InputField { get; private set; }

		/// <summary>The TextInputHandler</summary>
		public TextInputHandler TextInputHandler { get; private set; }

		/// <summary>The TextNavigator</summary>
		public TextNavigator TextNavigator { get; private set; }

		/// <summary>The Canvas</summary>
		public Canvas Canvas { get { return InputField.Canvas; } }

		/// <summary>Indicates if the ActionBar is visible</summary>
		public bool Visible { get { return gameObject.activeInHierarchy; } }

		/// <summary>Indicates if the cut operation is enabled</summary>
		private bool cut;

		/// <summary>Indicates if the copy operation is enabled</summary>
		private bool copy;

		/// <summary>Indicates if the paste operation is enabled</summary>
		private bool paste;

		/// <summary>Indicates if the select all operation is enabled</summary>
		private bool selectAll;

		/// <summary>All active actions</summary>
		private List<ActionBarAction> actions;

		/// <summary>The default actions: cut, copy, paste, select all</summary>
		private List<ActionBarAction> defaultActions;

		/// <summary>The replace actions</summary>
		private List<ActionBarAction> replaceActions;

		/// <summary>The custom actions</summary>
		private List<ActionBarAction> customActions;

		/// <summary>Current action index used for pages</summary>
		private int actionIndex;

		/// <summary>The text used for the cut action</summary>
		public string CutText { get; set; }

		/// <summary>The text used for the copy action</summary>
		public string CopyText { get; set; }

		/// <summary>The text used for the paste action</summary>
		public string PasteText { get; set; }

		/// <summary>The text used for the select all action</summary>
		public string SelectAllText { get; set; }

		/// <summary>The text used for the replace action</summary>
		public string ReplaceText { get; set; }

		/// <summary>Indicates if the default actions should be placed before the custom actions</summary>
		public bool DefaultActionsFirst
		{
			get { return defaultActionsFirst; }
			set
			{
				if(defaultActionsFirst != value)
				{
					defaultActionsFirst = value;
					RefreshActions();
					LoadPage();
				}
			}
		}

		/// <summary>Show the replace action immediately (not in the "Replace..." submenu) if there is only one</summary>
		public bool ShowSoloReplaceActionImmediately
		{
			get { return showSoloReplaceActionImmediately; }
			set
			{
				if(showSoloReplaceActionImmediately != value)
				{
					showSoloReplaceActionImmediately = value;
					RefreshActions();
					LoadPage();
				}
			}
		}

		/// <summary>Initializes this class</summary>
		internal void Initialize(AdvancedInputField inputField, TextInputHandler textInputHandler, TextNavigator textNavigator)
		{
			InputField = inputField;
			TextInputHandler = textInputHandler;
			TextNavigator = textNavigator;

			if(Canvas != null)
			{
				UpdateSize(Canvas.scaleFactor);
				actionBarRenderer.Initialize();
			}
		}

		#region UNITY
		private void Awake()
		{
			RectTransform = GetComponent<RectTransform>();
			itemPool = actionBarRenderer.transform.Find("Items").GetComponentsInChildren<ActionBarItem>();
			dividerPool = actionBarRenderer.transform.Find("Dividers").GetComponentsInChildren<Image>();
			actions = new List<ActionBarAction>();
			defaultActions = new List<ActionBarAction>();
			replaceActions = new List<ActionBarAction>();
			customActions = new List<ActionBarAction>();
			gameObject.SetActive(false);
		}

		private void OnEnable()
		{
			actionBarRenderer.Show();

			int length = itemPool.Length;
			for(int i = 0; i < length; i++)
			{
				itemPool[i].Click += OnItemClick;
			}
		}

		private void OnDisable()
		{
			actionBarRenderer.Hide();

			int length = itemPool.Length;
			for(int i = 0; i < length; i++)
			{
				itemPool[i].Click -= OnItemClick;
			}
		}

		private void Update()
		{
			actionBarRenderer.SyncTransform(RectTransform);
		}
		#endregion

		/// <summary>Updates the replace actions with given actions.</summary>
		public void UpdateReplaceActions(List<ActionBarAction> replaceActions)
		{
			this.replaceActions = replaceActions;
			RefreshActions();
			LoadPage();
		}

		/// <summary>Updates the custom actions with given actions.</summary>
		public void UpdateCustomActions(List<ActionBarAction> customActions)
		{
			this.customActions = customActions;
			RefreshActions();
			LoadPage();
		}

		public void OnNextPageClick()
		{
			actionIndex += MAX_ACTIONS_PER_PAGE;
			LoadPage();
		}

		public void OnPreviousPageClick()
		{
			actionIndex -= MAX_ACTIONS_PER_PAGE;
			LoadPage();
		}

		public void OnItemClick(ActionBarItem item)
		{
			ActionBarAction action = item.Action;
			switch(item.Action.type)
			{
				case ActionBarActionType.COPY: TextInputHandler.OnCopy(); break;
				case ActionBarActionType.CUT: TextInputHandler.OnCut(); break;
				case ActionBarActionType.PASTE: TextInputHandler.OnPaste(); break;
				case ActionBarActionType.SELECT_ALL: TextInputHandler.OnSelectAll(); break;
				case ActionBarActionType.SHOW_REPLACE: ShowReplaceActions(); break;
				case ActionBarActionType.REPLACE: TextInputHandler.OnReplace(action.text); break;
				case ActionBarActionType.CUSTOM:
					if(action.onClick != null)
					{
						action.onClick(action);
					}
					else
					{
						Debug.LogWarningFormat("Custom action {0} clicked without event listener, please specify an event listener in ActionBarAction constructor", action.text);
					}
					break;
			}
		}

		/// <summary>Determines fullSize and buttonSize</summary>
		internal void UpdateSize(float canvasScaleFactor)
		{
			if(RectTransform == null)
			{
				RectTransform = GetComponent<RectTransform>();
			}

#if UNITY_EDITOR || UNITY_STANDALONE
			int thumbSize = -1;
#else
			int thumbSize = Util.DetermineThumbSize();
#endif
			float cursorSize;
			if(thumbSize <= 0) //Unknown DPI
			{
				if(InputField.TextRenderer.ResizeTextForBestFit)
				{
					cursorSize = InputField.TextRenderer.FontSizeUsedForBestFit * 1.5f;
				}
				else
				{
					cursorSize = InputField.TextRenderer.FontSize * 1.5f;
				}
			}
			else
			{
				cursorSize = (thumbSize * THUMB_SIZE_RATIO) / canvasScaleFactor;
			}

			float canvasMinSize = Mathf.Min(Canvas.pixelRect.width, Canvas.pixelRect.height);
			fullSize = new Vector2((canvasMinSize * CANVAS_MIN_SIZE_RATIO) / canvasScaleFactor, cursorSize);
			RectTransform.sizeDelta = fullSize;

			buttonSize = new Vector2(fullSize.x / MAX_ACTIONS_IN_BAR, 0);

			actionBarRenderer.RefreshCanvas(Canvas);
			actionBarRenderer.SyncTransform(RectTransform);
		}

		/// <summary>Shows the ActionBar</summary>
		/// <param name="cut">Indicates if the cut button should be enabled</param>
		/// <param name="copy">Indicates if the copy button should be enabled</param>
		/// <param name="paste">Indicates if the paste button should be enabled</param>
		/// <param name="selectAll">Indicates if the select all button should be enabled</param>
		public void Show(bool cut, bool copy, bool paste, bool selectAll)
		{
			if(Visible && this.cut == cut && this.copy == copy && this.paste == paste && this.selectAll == selectAll)
			{
				return;
			}
			else
			{
				this.cut = cut;
				this.copy = copy;
				this.paste = paste;
				this.selectAll = selectAll;
			}

			gameObject.SetActive(true);

			if(Canvas != null)
			{
				UpdateSize(Canvas.scaleFactor);
			}

			UpdateButtons();
		}

		internal void RefreshActions()
		{
			actions.Clear();

			if(defaultActionsFirst)
			{
				AddDefaultActions();
				AddReplaceActions();
				actions.AddRange(customActions);
			}
			else
			{
				actions.AddRange(customActions);
				AddDefaultActions();
				AddReplaceActions();
			}

			actionIndex = 0;
		}

		internal void AddDefaultActions()
		{
			if(cut) { actions.Add(new ActionBarAction(ActionBarActionType.CUT, CutText)); }
			if(copy) { actions.Add(new ActionBarAction(ActionBarActionType.COPY, CopyText)); }
			if(paste) { actions.Add(new ActionBarAction(ActionBarActionType.PASTE, PasteText)); }
			if(selectAll) { actions.Add(new ActionBarAction(ActionBarActionType.SELECT_ALL, SelectAllText)); }
		}

		internal void AddReplaceActions()
		{
			if(replaceActions.Count > 0)
			{
				if(replaceActions.Count == 1 && showSoloReplaceActionImmediately)
				{
					actions.AddRange(replaceActions);
				}
				else
				{
					actions.Add(new ActionBarAction(ActionBarActionType.SHOW_REPLACE, ReplaceText));
				}
			}
		}

		internal void ShowReplaceActions()
		{
			actions.Clear();
			actions.AddRange(replaceActions);
			actionIndex = 0;

			LoadPage();
		}

		internal void LoadPage()
		{
			bool exactFit = (actions.Count == MAX_ACTIONS_IN_BAR);
			bool hasPreviousPage = (!exactFit && actionIndex > 0);
			bool hasNextPage = (!exactFit && (actionIndex + MAX_ACTIONS_PER_PAGE) < actions.Count);
			int maxActionsOnPage = MAX_ACTIONS_PER_PAGE;
			if(exactFit) { maxActionsOnPage = MAX_ACTIONS_IN_BAR; }
			float x = 0;

			previousPageButton.gameObject.SetActive(hasPreviousPage);
			if(hasPreviousPage)
			{
				RectTransform buttonTransform = previousPageButton.GetComponent<RectTransform>();
				Vector2 sizeDelta = buttonTransform.sizeDelta;
				sizeDelta.x = (buttonSize.x * 0.5f);
				buttonTransform.sizeDelta = sizeDelta;
				buttonTransform.anchoredPosition = new Vector2(x, 0);

				x += buttonTransform.rect.width;
			}

			int itemsLength = itemPool.Length;
			for(int i = 0; i < itemsLength; i++)
			{
				itemPool[i].gameObject.SetActive(false);
			}

			int dividersLength = dividerPool.Length;
			for(int i = 0; i < dividersLength; i++)
			{
				dividerPool[i].gameObject.SetActive(false);
			}

			int dividerIndex = 0;

			int actionsLength = actions.Count;
			for(int i = 0; i < maxActionsOnPage; i++)
			{
				ActionBarItem item = itemPool[i];

				int currentActionIndex = actionIndex + i;
				if(currentActionIndex >= actionsLength)
				{
					break;
				}

				if(showDividers && (hasPreviousPage || i > 0))
				{
					Image divider = dividerPool[dividerIndex];
					divider.gameObject.SetActive(true);
					divider.rectTransform.anchoredPosition = new Vector2(x, 0);
					dividerIndex++;
				}

				item.gameObject.SetActive(true);
				item.ConfigureUI(actions[currentActionIndex]);
				RectTransform buttonTransform = item.GetComponent<RectTransform>();
				Vector2 sizeDelta = buttonTransform.sizeDelta;
				sizeDelta.x = buttonSize.x;
				buttonTransform.sizeDelta = sizeDelta;
				buttonTransform.anchoredPosition = new Vector2(x, 0);

				x += buttonTransform.rect.width;
			}

			nextPageButton.gameObject.SetActive(hasNextPage);
			if(hasNextPage)
			{
				if(showDividers)
				{
					Image divider = dividerPool[dividerIndex];
					divider.gameObject.SetActive(true);
					divider.rectTransform.anchoredPosition = new Vector2(x, 0);
				}

				RectTransform buttonTransform = nextPageButton.GetComponent<RectTransform>();
				Vector2 sizeDelta = buttonTransform.sizeDelta;
				sizeDelta.x = (buttonSize.x * 0.5f);
				buttonTransform.sizeDelta = sizeDelta;
				buttonTransform.anchoredPosition = new Vector2(x, 0);

				x += buttonTransform.rect.width;
			}

			RectTransform.sizeDelta = new Vector2(Mathf.Abs(x), fullSize.y);

			if(TextNavigator != null)
			{
				TextNavigator.KeepActionBarWithinBounds();
			}
		}

		internal void UpdateButtons()
		{
			if(defaultActions == null) { return; }

			RefreshActions();
			LoadPage();

			if(Canvas != null)
			{
				actionBarRenderer.RefreshCanvas(Canvas);
				actionBarRenderer.SyncTransform(RectTransform);
			}
		}

		/// <summary>Changes the position of the ActionBar</summary>
		/// <param name="position">The new position of the ActionBar</param>
		internal void UpdatePosition(Vector2 position)
		{
			if(Canvas != null)
			{
				RectTransform.anchoredPosition = position;
				RectTransform.SetAsLastSibling();

				actionBarRenderer.RefreshCanvas(Canvas);
				actionBarRenderer.SyncTransform(RectTransform);
			}
		}

		/// <summary>Hides the ActionBar</summary>
		public void Hide()
		{
			gameObject.SetActive(false);
			actionBarRenderer.gameObject.SetActive(false);
		}
	}

	public struct ActionBarAction
	{
		public ActionBarActionType type;
		public string text;
		public Action<ActionBarAction> onClick;

		public ActionBarAction(ActionBarActionType type, string text, Action<ActionBarAction> onClick = null)
		{
			this.type = type;
			this.text = text;
			this.onClick = onClick;
		}
	}
}