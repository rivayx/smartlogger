﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using AdvancedInputFieldPlugin;

public class AdvancedKeyboardScroller : MonoBehaviour
{
    [SerializeField]
    private KeyboardScrollMode keyboardScrollMode;

    [SerializeField]
    private RectTransform contentRect;

    [SerializeField]
    private RectTransform bottomScrollPivot;

    [SerializeField]
    [Range(0.1f, 1f)]
    private float transitionTime = 0.1f;

    [SerializeField]
    [Range(0f, 1f)]
    private float normalizedOffsetY = 0.025f;

    [SerializeField]
    private bool disableScrollAtReturn = false;

    [SerializeField]
    private bool limitScrollMinY = false;

    private Vector2 originalContentPosition;
    private Vector2 startContentPosition;
    private Vector2 endContentPosition;
    private float lastMoveY;
    private float currentTime;
    private int lastKeyboardHeight;
    private bool isKeyboardOpen;
    private Canvas canvas;
    

    public Canvas Canvas
    {
        get
        {
            if (canvas == null)
            {
                canvas = GetComponentInParent<Canvas>();
            }

            return canvas;
        }
    }

    public void SetBottomScrollPivot(RectTransform rectTransform)
    {
        bottomScrollPivot = rectTransform;
    }

    // Start is called before the first frame update
    void Start()
    {
        originalContentPosition = contentRect.anchoredPosition;
    }

    // Update is called once per frame
    void Update()
    {
        if (currentTime < transitionTime)
        {
            currentTime += Time.deltaTime;
            if (currentTime >= transitionTime)
            {
                currentTime = transitionTime;
            }

            float progress = currentTime / transitionTime;
            contentRect.anchoredPosition = Vector2.Lerp(startContentPosition, endContentPosition, progress);
        }
    }

    private void OnEnable()
    {
        NativeKeyboardManager.AddKeyboardHeightChangedListener(OnKeyboardHeightChanged);
    }

    private void OnDisable()
    {
        contentRect.anchoredPosition = originalContentPosition;
        NativeKeyboardManager.RemoveKeyboardHeightChangedListener(OnKeyboardHeightChanged);
    }

    public void OnKeyboardHeightChanged(int keyboardHeight)
    {
        if (keyboardHeight > 0)
        {
            ScrollContent(keyboardHeight);
        }
        else
        {
            ScrollBack();
        }

        lastKeyboardHeight = keyboardHeight;
    }

    private void GetNormalizedVertical(RectTransform rectTransform, out float normalizedY, out float normalizedHeight)
    {
        Vector3[] corners = new Vector3[4];
        rectTransform.GetWorldCorners(corners);

        float bottomY = corners[1].y;
        if (Canvas.renderMode == RenderMode.ScreenSpaceOverlay)
        {
            normalizedY = bottomY / Screen.height;
        }
        else
        {
            Camera camera = Canvas.worldCamera;
            normalizedY = (bottomY + camera.orthographicSize) / (camera.orthographicSize * 2);
        }
        normalizedY -= normalizedOffsetY;

        Vector2 size = new Vector2(Mathf.Abs(corners[3].x - corners[1].x), Mathf.Abs(corners[3].y - corners[1].y));
        if (Canvas.renderMode == RenderMode.ScreenSpaceOverlay)
        {
            normalizedHeight = size.y / Screen.height;
        }
        else
        {
            Camera camera = Canvas.worldCamera;
            normalizedHeight = size.y / (camera.orthographicSize * 2);
        }
    }

    public void ScrollContent(int keyboardHeight)
    {
        //lastMoveY = 0f;
        GameObject targetObject = EventSystem.current.currentSelectedGameObject;
        if (targetObject == null || targetObject.GetComponent<AdvancedInputField>() == null)
        {
            return;
        }

        if (targetObject.GetComponent<AdvancedInputField>() != null)
        {
            //RectTransform scrollTarget = targetObject.GetComponent<RectTransform>();
            RectTransform scrollTarget = bottomScrollPivot;
            float normalizedY, normalizedHeight;
            GetNormalizedVertical(scrollTarget, out normalizedY, out normalizedHeight);
            float normalizedKeyboardHeight = keyboardHeight / (float)Screen.height;

            float moveY = (normalizedKeyboardHeight - (normalizedY - normalizedHeight)) * (Canvas.pixelRect.height / Canvas.scaleFactor);

            if (keyboardScrollMode == KeyboardScrollMode.ONLY_SCROLL_IF_INPUTFIELD_BLOCKED
                && (moveY < 0 && lastMoveY < 0))
            {
                return;
            }

            startContentPosition = contentRect.anchoredPosition;
            endContentPosition = startContentPosition;

            if (limitScrollMinY)
            {
                if ((endContentPosition.y + moveY) < 0f)
                {
                    moveY = 0f;
                    endContentPosition.y = 0f;
                }
            }

            lastMoveY = moveY;

            endContentPosition.y += moveY;

            currentTime = 0;

            isKeyboardOpen = true;
        }
    }

    public void ScrollBack()
    {
        if (disableScrollAtReturn)
        {
            startContentPosition = contentRect.anchoredPosition;
            endContentPosition = contentRect.anchoredPosition; //Keep forcing current position while scrollrect resizes
        }
        else
        {
            startContentPosition = contentRect.anchoredPosition;
            endContentPosition = originalContentPosition; //return the original position for disable content scroll vertical
        }
        currentTime = 0;
        isKeyboardOpen = false;
    }
}
