﻿using AdvancedInputFieldPlugin;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Smartlogger.Popup;

public class AuthenticateManager : SceneManager
{
    [SerializeField] AdvancedInputField emailInputField;
    [SerializeField] AdvancedInputField passwordInputField;

    protected override void Start()
    {
        base.Start();
        emailInputField.Text = "";
        passwordInputField.Text = "";
    }

    public void LoginOnClick()
    {
        Popup.Instance.ShowPopup(new PopupParameter()
        {
            title = "Sukses",
            content = "Bisa masuk ke popup",
            buttonYesAction = ()=> Debug.Log("YES JALAN"),
            buttonNoAction = ()=> Debug.Log("No JALAN"),
        });
        //LoadScene(SceneName.Home);
    }

    public void RegisterOnClick()
    {
        LoadScene(SceneName.Register);
    }

    public void ForgetPasswordOnClick()
    {
        
    }
}
