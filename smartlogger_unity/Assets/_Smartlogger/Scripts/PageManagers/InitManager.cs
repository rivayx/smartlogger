﻿using UnityEngine;

public class InitManager : SceneManager
{
    protected override void Start()
    {
        base.Start();
        //UserData.Instance.Save("rivayx@gmail.com", "bojonggede");
        //UserData.Instance.Delete();

        UserData.Instance.Load((isLoaded) =>
        {
            if (isLoaded)
            {
                LoadScene(SceneName.Home);
            }
            else
            {
                LoadScene(SceneName.Authenticate);
            }
        });
    }
}
