﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ItemMonitoringVariable : MonoBehaviour
{
    public TMP_Text title;
    public TMP_Text value;
}
