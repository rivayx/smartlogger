﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Lean.Pool;

public class DeviceItemManager : MonoBehaviour
{
    [SerializeField] TMP_Text deviceName;
    [SerializeField] Button buttonDeviceAction;

    [SerializeField] Image wifiDisconnect;
    [SerializeField] Image wifiSignal;
    [SerializeField] List<Sprite> spriteSignal;
    [SerializeField] TMP_Text wifiSSID;

    [SerializeField] TMP_Text dateLastUpdate;
    [SerializeField] TMP_Text timeLastUpdate;

    [SerializeField] LeanGameObjectPool pollerItemMonitoring;

    public void CreateData(JSONDataGetUserDevice data)
    {
        deviceName.text = data.device_name;

        wifiSSID.text = data.wifi_ssid;
        wifiSignal.sprite = spriteSignal[data.wifi_signal];
        wifiDisconnect.gameObject.SetActive(data.wifi_signal == 0);

        //dateLastUpdate.text = 

        foreach (JSONMonitoringDetailGetUserDevice monitoring in data.last_monitoring.monitoring_detail)
        {
            GameObject itemMonitoring = pollerItemMonitoring.Spawn(pollerItemMonitoring.transform);
            ItemMonitoringVariable monitoringVariable = itemMonitoring.GetComponent<ItemMonitoringVariable>();

            monitoringVariable.title.text = monitoring.sensor_name;
            monitoringVariable.value.text = monitoring.val.ToString() + monitoring.satuan;
        }
    }


}
