﻿using System;

[Serializable]
public class JsonResponseAuthenticate
{
    public string access_token;
}

[Serializable]
public class JsonResponseUserInfo
{
    public UserInfo user_info;
}