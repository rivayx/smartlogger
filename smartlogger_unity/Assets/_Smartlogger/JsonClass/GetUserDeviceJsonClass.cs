﻿using System;
using System.Collections.Generic;

// JSONResponseGetUserDevice myDeserializedClass = JsonConvert.DeserializeObject<JSONResponseGetUserDevice>(myJsonResponse); 
[Serializable]
public class JSONResponseGetUserDevice
{
    public string status;
    public int status_code;
    public string message;
    public List<JSONDataGetUserDevice> data;
}

[Serializable]
public class JSONDataGetUserDevice
{
    public string device_id;
    public string version_id;
    public string device_name;
    public string wifi_ssid;
    public string wifi_password;
    public int wifi_signal;
    public string send_data_now;
    public JSONLastMonitoringGetUserDevice last_monitoring;
}

[Serializable]
public class JSONLastMonitoringGetUserDevice
{
    public string last_update;
    public string monitoring_id;
    public List<JSONMonitoringDetailGetUserDevice> monitoring_detail;
}

[Serializable]
public class JSONMonitoringDetailGetUserDevice
{
    public string sensor_name;
    public float val;
    public string satuan;
}

