﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System;
using System.Collections;

namespace Smartlogger.CustomScrollRect
{
    public class CustomScrollRect : ScrollRect
    {
        public const float MaximumPullDistance = -180;

        public float pullDistanceRefreshThreshold = -150;

        private bool isDragging;
        private bool isAbleToRefresh;
        private bool isRefreshing;

        public delegate void Refreshing(Action onCompleted);
        public Refreshing OnRefreshing;

        public delegate void Refreshed();
        public Refreshed OnRefreshed;

        public delegate void Pulling(float posY, float velocityY);
        public Pulling OnPulling;

        public void DelayEnable(float val)
        {
            StartCoroutine(WaitEnable(val));
        }

        IEnumerator WaitEnable(float val)
        {
            yield return new WaitForSeconds(val);
            this.enabled = true;
        }

        protected override void Awake()
        {
            base.Awake();

            onValueChanged.AddListener(OnScrolling);
        }
        public override void OnBeginDrag(PointerEventData eventData)
        {
            base.OnBeginDrag(eventData);

            isDragging = true;
        }

        public override void OnEndDrag(PointerEventData eventData)
        {
            base.OnEndDrag(eventData);

            isDragging = false;
        }

        protected override void LateUpdate()
        {
            base.LateUpdate();

            // Limit pulling distance
            //if (content.anchoredPosition.y <= MaximumPullDistance)
            //{
            //    Vector2 tempPos = content.anchoredPosition;
            //    tempPos.y = -120f;
            //    content.anchoredPosition = tempPos;
            //}

            if (isRefreshing)
            {
                HoldContentPositionAt(pullDistanceRefreshThreshold);
            }
        }

        private void OnScrolling(Vector2 value)
        {
            float anchorPosY = GetContentAnchoredPosition().y;

            if (IsInPulledPosition() && !isRefreshing)
            {
                OnPulling(anchorPosY, velocity.y);
            }

            if (isDragging)
            {
                if (anchorPosY <= pullDistanceRefreshThreshold)
                {
                    isAbleToRefresh = true;
                }
                else
                {
                    isAbleToRefresh = false;
                }
            }
            else if (!isDragging)
            {
                if (isAbleToRefresh)
                {
                    isAbleToRefresh = false;
                    isRefreshing = true;

                    OnRefreshing?.Invoke(OnRefreshingComplete);
                }
            }
        }

        private void OnRefreshingComplete()
        {
            isRefreshing = false;

            OnRefreshed?.Invoke();
        }

        private void HoldContentPositionAt(float posY)
        {
            float anchorPosY = GetContentAnchoredPosition().y;
            if (anchorPosY >= posY)
            {
                Vector2 tempPos = GetContentAnchoredPosition();
                tempPos.y = posY;
                content.anchoredPosition = tempPos;
            }
        }

        private Vector2 GetContentAnchoredPosition()
        {
            return content.anchoredPosition;
        }

        private bool IsInPulledPosition()
        {
            float anchorPosY = GetContentAnchoredPosition().y;

            return (anchorPosY <= -1);
        }
    }
}
