﻿using System;
using System.Collections.Generic;
using System.Collections;
using UnityEngine;

namespace Smartlogger.Popup
{
    public class PopupParameter
    {
        public string title;
        public string content;
        public string buttonYesLabel;
        public string buttonNoLabel;
        public Action buttonYesAction;
        public Action buttonNoAction;

        public PopupParameter()
        {
        }

        public PopupParameter(string title, string content, string buttonYesLabel = null, string buttonNoLabel = null, 
                                Action buttonYesAction = null, Action buttonNoAction = null)
        {
            this.title = title;
            this.content = content;
            this.buttonYesLabel = buttonYesLabel;
            this.buttonNoLabel = buttonNoLabel;
            this.buttonYesAction = buttonYesAction;
            this.buttonNoAction = buttonNoAction;
        }

    }
    
}
