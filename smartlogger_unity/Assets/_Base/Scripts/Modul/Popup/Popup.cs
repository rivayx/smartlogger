﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Smartlogger.Popup;


public class Popup : Singleton<Popup>
{
    [SerializeField] GameObject panelPopup;

    [SerializeField] Text title;
    [SerializeField] Text contentPopup;

    [SerializeField] Button buttonYes;
    [SerializeField] Text labelButtonYes;
    [SerializeField] Button buttonNo;
    [SerializeField] Text labelButtonNo;

    private void Start()
    {
        panelPopup.SetActive(false);
    }

    public void ShowPopup(PopupParameter popupParameter)
    {
        panelPopup.SetActive(true);

        labelButtonYes.text = "OK";
        labelButtonNo.text = "Batal";
        title.text = "";

        buttonYes.onClick.RemoveAllListeners();
        buttonNo.onClick.RemoveAllListeners();

        buttonYes.gameObject.SetActive(false);
        buttonNo.gameObject.SetActive(false);

        if (popupParameter == null)
            return;

        if (!string.IsNullOrEmpty(popupParameter.title))
            title.text = popupParameter.title;

        if (!string.IsNullOrEmpty(popupParameter.buttonYesLabel))
            labelButtonYes.text = popupParameter.buttonYesLabel;

        if (!string.IsNullOrEmpty(popupParameter.buttonNoLabel))
            labelButtonNo.text = popupParameter.buttonNoLabel;

        contentPopup.text = popupParameter.content;

        buttonYes.gameObject.SetActive(true);
        buttonYes.onClick.AddListener(() =>
        {
            if (popupParameter.buttonYesAction != null)
                popupParameter.buttonYesAction();

            ClosePopup();
        });


        if (popupParameter.buttonYesAction != null && popupParameter.buttonNoAction != null)
        {
            buttonNo.gameObject.SetActive(true);
            buttonNo.onClick.AddListener(() =>
            {
                if (popupParameter.buttonNoAction != null)
                    popupParameter.buttonNoAction();

                ClosePopup();
            });
        }
    }

    public void ClosePopup()
    {
        panelPopup.SetActive(false);
    }
}
