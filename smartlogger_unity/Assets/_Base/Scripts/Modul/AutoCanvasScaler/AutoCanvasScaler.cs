﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class AutoCanvasScaler : MonoBehaviour
{
    // HOW TO USE
    /*
    1. Just put this script on canvas
    2. Voila it works like a charm!
     */

    // Start is called before the first frame update
    void Start()
    {
        RefreshMatchCanvasScaler();
    }

    public void RefreshMatchCanvasScaler()
    {
        int width = Screen.width;
        int height = Screen.height;
        int modulus = GetModulus(width, height);
        if (modulus == 0) return;
        int ratioWidth = (width / modulus);
        int ratioHeight = (height / modulus);

        GetComponent<CanvasScaler>().matchWidthOrHeight = 0f;
        if (ratioWidth==3)
        {
            GetComponent<CanvasScaler>().matchWidthOrHeight = 1f;
        }

        // Debug.Log("Screen Ratio: " + ratioWidth + ", " + ratioHeight);
    }

    public void RefreshMatchCanvasScaler(ScreenOrientation screenOrientation, float portraitVal = 0f, float landscapeVal = 1f)
    {
        StartCoroutine(DelayRefreshMatchCanvasScaler(screenOrientation, portraitVal, landscapeVal));
    }

    IEnumerator DelayRefreshMatchCanvasScaler(ScreenOrientation screenOrientation, float portraitVal = 0f, float landscapeVal = 1f)
    {
        yield return new WaitForEndOfFrame();
        if (screenOrientation == ScreenOrientation.Portrait)
            GetComponent<CanvasScaler>().matchWidthOrHeight = portraitVal;
        else if (screenOrientation == ScreenOrientation.Landscape)
        {
            GetComponent<CanvasScaler>().matchWidthOrHeight = landscapeVal;
        }
    }

    private int GetModulus(int a, int b)
    {
        if (b > a)
        {
            return b % a;
        }
        else
        {
            return a % b;
        }
    }
}
