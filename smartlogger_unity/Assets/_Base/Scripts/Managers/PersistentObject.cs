﻿using UnityEngine;

public class PersistentObject : MonoBehaviour
{
    public static PersistentObject Instance;

    private void Awake()
    {

        if (Instance != null && Instance != this)
        {
            Destroy(this.gameObject);
        }
        else 
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
            
    }
}
