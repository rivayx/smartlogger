﻿using Newtonsoft.Json;
using Smartlogger.CallApi;
using Smartlogger.Popup;
using System;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;

public class UserData : Singleton<UserData>
{
    public string accessToken;
    public UserInfo account = new UserInfo();
    public List<DataDevice> listDevice = new List<DataDevice>();

    public void Save(string email, string password)
    {
        EncryptedPlayerPrefs.SetString(Key.Email, email);
        EncryptedPlayerPrefs.SetString(Key.Password, password);
    }

    public void Delete()
    {
        EncryptedPlayerPrefs.DeleteKey(Key.Email);
        EncryptedPlayerPrefs.DeleteKey(Key.Password);
    }

    public void Load(Action<bool> isLoaded)
    {
        string email = EncryptedPlayerPrefs.GetString(Key.Email);
        string password = EncryptedPlayerPrefs.GetString(Key.Password);

        if (!string.IsNullOrEmpty(email) && !string.IsNullOrEmpty(password))
        {
            WWWForm formAuthenticate = new WWWForm();
            formAuthenticate.AddField("email", email);
            formAuthenticate.AddField("password", password);
            WebRequest.Request.Post(URL.Authenticate, formAuthenticate, (resAuth) =>
            {
                BaseJsonClass baseJsonAuthenticate = JsonConvert.DeserializeObject<BaseJsonClass>(resAuth);
                if (baseJsonAuthenticate.status_code == 10)
                {
                    accessToken = JsonConvert.DeserializeObject<JsonResponseAuthenticate>(resAuth).access_token;

                    WWWForm formUserInfo = new WWWForm();
                    formUserInfo.AddField("access_token", accessToken);
                    WebRequest.Request.Post(URL.UserInfo, formUserInfo, (resUserInfo) =>
                    {
                        BaseJsonClass baseJsonUserInfo = JsonConvert.DeserializeObject<BaseJsonClass>(resUserInfo);
                        if (baseJsonAuthenticate.status_code == 10)
                        {
                            account = JsonConvert.DeserializeObject<JsonResponseUserInfo>(resUserInfo).user_info;
                            isLoaded(true);
                        }
                        else
                        {
                            Popup.Instance.ShowPopup(new PopupParameter()
                            {
                                title = "Gagal",
                                content = baseJsonUserInfo.message,
                                buttonYesLabel = "Tutup"
                            });
                            isLoaded(false);
                        }
                    });
                }
                else
                {
                    Popup.Instance.ShowPopup(new PopupParameter()
                    {
                        title = "Gagal",
                        content = baseJsonAuthenticate.message,
                        buttonYesLabel = "Tutup"
                    });
                    isLoaded(false);
                }
            });
        }
        else
        {
            isLoaded(false);
        }
    }
}

