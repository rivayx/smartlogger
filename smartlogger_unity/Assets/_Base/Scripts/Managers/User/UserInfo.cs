﻿using System;

[Serializable]
public class UserInfo
{
    public string email;
    public string name;
    public string phone;
    public string address;

    public UserInfo()
    {
        
    }
}

