﻿using System.Globalization;

public static class SceneName
{
    public const string Authenticate = "Authenticate";
    public const string Register = "Register";
    public const string Home = "Home";
    public const string Monitoring = "Monitoring";
}