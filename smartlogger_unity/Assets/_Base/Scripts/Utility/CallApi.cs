﻿using System;
using UnityEngine;
using UnityEngine.Networking;
using System.Collections.Generic;
using System.Collections;

namespace Smartlogger.CallApi
{
    public static class URL
    {
        public const string Host =
            //"https://api.bojonggededeveloper.com/smartlogger/";
            "http://192.168.100.2/smartlogger/api/";

        public const string User = Host + "user/";
        public const string Register = User + "register";
        public const string Authenticate = User + "authenticate";
        public const string UserInfo = User + "user_info";
        public const string EditUser = User + "edit_user";
    }

    public class WebRequest : MonoBehaviour
    {
        private UnityWebRequest webRequest;
        private Action<float> asyncCaller;

        public static WebRequest Request
        {
            get
            {
                GameObject go = new GameObject("WebRequest");
                go.AddComponent<WebRequest>();

                return go.GetComponent<WebRequest>();
            }
        }

        public void Get(string url, Action<string> response, Action<float> asyncAction = null)
        {
            asyncCaller = asyncAction;
            StartCoroutine(GetEnumerator(url, response));
        }

        public void Post(string url, WWWForm form, Action<string> response, Action<float> asyncAction = null)
        {
            asyncCaller = asyncAction;
            StartCoroutine(PostEnumerator(url, form, response));
        }

        private void Update()
        {
            if (webRequest != null)
            {
                if (asyncCaller != null)
                {
                    asyncCaller(webRequest.downloadProgress);
                }
            }
        }

        IEnumerator GetEnumerator(string url, Action<string> response)
        {
            webRequest = UnityWebRequest.Get(url);
            yield return webRequest.SendWebRequest();

            if (webRequest.error != null)
            {
                response(null);
                Debug.LogError(webRequest.error);
            }
            else
            {
                response(webRequest.downloadHandler.text);
            }
            Destroy(gameObject);
        }

        IEnumerator PostEnumerator(string url, WWWForm form, Action<string> response)
        {
            webRequest = UnityWebRequest.Post(url, form);
            yield return webRequest.SendWebRequest();

            if (webRequest.error != null)
            {
                response(null);
                Debug.LogError(webRequest.error);
            }
            else
            {
                response(webRequest.downloadHandler.text);
            }
            Destroy(gameObject);
        }
    }
}