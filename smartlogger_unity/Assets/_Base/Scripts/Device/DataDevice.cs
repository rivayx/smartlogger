﻿using System;

[Serializable]
public class DataDevice
{
    public string id = string.Empty;
    public string version = string.Empty;
    public string name = string.Empty;
    public string ssid = string.Empty;
    public int signal = 0;
    public float temperature = 0f;
    public float humidity = 0f;
    public string lastUpdate = string.Empty;

    public DataDevice()
    {
        
    }

}
